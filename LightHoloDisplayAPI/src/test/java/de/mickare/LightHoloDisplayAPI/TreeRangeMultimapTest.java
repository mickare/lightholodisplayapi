package de.mickare.LightHoloDisplayAPI;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import de.mickare.LightHoloDisplayAPI.util.collect.TreeRangeMultimap;
import com.google.common.collect.Range;
import junit.framework.TestCase;

public class TreeRangeMultimapTest extends TestCase {

	/**
	 * Create the test case
	 * 
	 * @param testName
	 *            name of the test case
	 */
	public TreeRangeMultimapTest(String testName) {
		super( testName );
	}

	public void testComparators() {
		Comparator<Range<Long>> c = new TreeRangeMultimap.RangeComparator<Long>();

		List<Range<Long>> ranges = new LinkedList<Range<Long>>();
		ranges.add( Range.<Long> all() );
		ranges.add( Range.atMost( (long) -10 ) );
		ranges.add( Range.lessThan( (long) -10 ) );
		ranges.add( Range.greaterThan( (long) 10 ) );
		ranges.add( Range.atLeast( (long) 10 ) );
		ranges.add( Range.closed( (long) -15, (long) 1 ) );
		ranges.add( Range.closed( (long) -1, (long) 15 ) );

		for(Range<Long> r1 : ranges) {
			for(Range<Long> r2 : ranges) {
				assertSame(c.compare( r1, r2 ), -c.compare( r2, r1 ));
			}
		}

	}
}
