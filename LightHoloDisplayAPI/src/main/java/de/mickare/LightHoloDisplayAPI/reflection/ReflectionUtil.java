package de.mickare.LightHoloDisplayAPI.reflection;

import java.lang.reflect.Field;

public class ReflectionUtil {

	/**
	 * Returns an object containing the value of any static field (even private).
	 * 
	 * @param className
	 *            The complete name of the class (ex. java.lang.String)
	 * @param fieldName
	 *            The name of a static field in the class
	 * @return An Object containing the static field value.
	 * @throws SecurityException .
	 * @throws NoSuchFieldException .
	 * @throws ClassNotFoundException .
	 * @throws IllegalArgumentException .
	 * @throws IllegalAccessException .
	 */
	public static Object getStaticValue( final String className, final String fieldName ) throws SecurityException, NoSuchFieldException,
			ClassNotFoundException, IllegalArgumentException, IllegalAccessException {
		return getStaticValue( Class.forName( className ), fieldName );
	}

	/**
	 * Returns an object containing the value of any static field (even private).
	 * 
	 * @param class The class (ex. java.lang.String)
	 * @param fieldName
	 *            The name of a static field in the class
	 * @return An Object containing the static field value.
	 * @throws SecurityException .
	 * @throws NoSuchFieldException .
	 * @throws ClassNotFoundException .
	 * @throws IllegalArgumentException .
	 * @throws IllegalAccessException .
	 */
	public static Object getStaticValue( final Class<?> c, final String fieldName ) throws SecurityException, NoSuchFieldException,
			ClassNotFoundException, IllegalArgumentException, IllegalAccessException {
		// Get the private field
		final Field field = c.getDeclaredField( fieldName );
		// Allow modification on the field
		field.setAccessible( true );
		try {
			// Return the Obect corresponding to the field
			return field.get( c );
		} finally {
			field.setAccessible( false );
		}
	}

	/**
	 * Use reflection to change value of any static field.
	 * 
	 * @param className
	 *            The complete name of the class (ex. java.lang.String)
	 * @param fieldName
	 *            The name of a static field in the class
	 * @param newValue
	 *            The value you want the field to be set to.
	 * @throws SecurityException .
	 * @throws NoSuchFieldException .
	 * @throws ClassNotFoundException .
	 * @throws IllegalArgumentException .
	 * @throws IllegalAccessException .
	 */
	public static void setStaticValue( final String className, final String fieldName, final Object newValue ) throws SecurityException,
			NoSuchFieldException, ClassNotFoundException, IllegalArgumentException, IllegalAccessException {
		setStaticValue( Class.forName( className ), fieldName, newValue );
	}

	/**
	 * Use reflection to change value of any static field.
	 * 
	 * @param className
	 *            The class (ex. java.lang.String)
	 * @param fieldName
	 *            The name of a static field in the class
	 * @param newValue
	 *            The value you want the field to be set to.
	 * @throws SecurityException .
	 * @throws NoSuchFieldException .
	 * @throws ClassNotFoundException .
	 * @throws IllegalArgumentException .
	 * @throws IllegalAccessException .
	 */
	public static void setStaticValue( final Class<?> c, final String fieldName, final Object newValue ) throws SecurityException,
			NoSuchFieldException, ClassNotFoundException, IllegalArgumentException, IllegalAccessException {
		// Get the private String field
		final Field field = c.getDeclaredField( fieldName );
		// Allow modification on the field
		field.setAccessible( true );
		try {
			// Get
			final Object oldValue = field.get( c );
			// Sets the field to the new value
			field.set( oldValue, newValue );
		} finally {
			field.setAccessible( false );
		}
	}

	// ******************************************
	// Integer

	public static int getStaticIntValue( final String className, final String fieldName ) throws SecurityException, NoSuchFieldException,
			ClassNotFoundException, IllegalArgumentException, IllegalAccessException {
		return getStaticIntValue( Class.forName( className ), fieldName );
	}

	public static int getStaticIntValue( final Class<?> c, final String fieldName ) throws SecurityException, NoSuchFieldException,
			ClassNotFoundException, IllegalArgumentException, IllegalAccessException {
		// Get the private field
		final Field field = c.getDeclaredField( fieldName );
		// Allow modification on the field
		field.setAccessible( true );
		try {
			// Return the Obect corresponding to the field
			return field.getInt( c );
		} finally {
			field.setAccessible( false );
		}
	}

	public static void setStaticIntValue( final String className, final String fieldName, final int newValue ) throws SecurityException,
			NoSuchFieldException, ClassNotFoundException, IllegalArgumentException, IllegalAccessException {
		setStaticIntValue( Class.forName( className ), fieldName, newValue );
	}

	public static void setStaticIntValue( final Class<?> c, final String fieldName, final int newValue ) throws SecurityException,
			NoSuchFieldException, ClassNotFoundException, IllegalArgumentException, IllegalAccessException {
		// Get the private String field
		final Field field = c.getDeclaredField( fieldName );
		// Allow modification on the field
		field.setAccessible( true );
		try {
			// Get
			final Object oldValue = field.get( c );
			// Sets the field to the new value
			field.setInt( oldValue, newValue );
		} finally {
			field.setAccessible( false );
		}
	}

	// ******************************************
	// Integer get And Increment

	public static int getAndIncrementStaticIntValue( final String className, final String fieldName, final int increment )
			throws SecurityException, NoSuchFieldException, ClassNotFoundException, IllegalArgumentException, IllegalAccessException {
		return getAndIncrementStaticIntValue( Class.forName( className ), fieldName, increment );
	}

	public static int getAndIncrementStaticIntValue( final Class<?> c, final String fieldName, final int increment )
			throws SecurityException, NoSuchFieldException, ClassNotFoundException, IllegalArgumentException, IllegalAccessException {
		// Get the private String field
		final Field field = c.getDeclaredField( fieldName );
		// Allow modification on the field
		field.setAccessible( true );
		try {
			// Get
			final Object oldValue = field.get( c );
			int old = field.getInt( c );
			// Sets the field to the new value
			field.setInt( oldValue, old + increment );
			return old;
		} finally {
			field.setAccessible( false );
		}
	}

}
