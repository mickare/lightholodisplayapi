package de.mickare.LightHoloDisplayAPI;

import com.comphenix.protocol.ProtocolManager;

import de.mickare.LightHoloDisplayAPI.hologram.HologramManager;

public class LHDAPI {
	
	private static LHD_Plugin static_plugin = null;
	
	protected static void initialize( LHD_Plugin plugin ) {
		static_plugin = plugin;
	}
	
	public static LHD_Plugin getPlugin() {
		return static_plugin;
	}
	
	public static HologramManager getManager() {
		return static_plugin.getHologramManager();
	}
	
	public static ProtocolManager getProtoManager() {
		return static_plugin.getProtocolManager();
	}
	
	public static void debug( String msg ) {
		static_plugin.debug( msg );
	}
	
	public static double getDefaultArmorStandVerticalOffset() {
		return static_plugin.getDefaultArmorStandVerticalOffset();
	}
	
	public static boolean isDefaultArmorStandSmall() {
		return static_plugin.isDefaultArmorStandSmall();
	}
	
	public static double getDefaultLineHeight() {
		return static_plugin.getDefaultVerticalLineSpacing();
	}
	
}
