package de.mickare.LightHoloDisplayAPI.util.concurrent;

import java.util.concurrent.locks.Lock;

public interface CloseableLock extends Lock, AutoCloseable {

	public CloseableLock open();
	
	@Override
	public void close();
	
}
