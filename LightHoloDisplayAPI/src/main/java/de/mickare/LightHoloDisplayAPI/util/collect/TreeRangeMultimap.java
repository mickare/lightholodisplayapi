package de.mickare.LightHoloDisplayAPI.util.collect;

import java.util.AbstractMap;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.Comparator;

import javax.annotation.Nullable;

import com.google.common.base.Preconditions;
import com.google.common.collect.BoundType;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultiset;
import com.google.common.collect.ImmutableRangeMap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multiset;
import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;

public class TreeRangeMultimap<K extends Comparable<K>, V> implements RangeMap<K, Multiset<V>> {

	public static final <K extends Comparable<K>, V> TreeRangeMultimap<K, V> create() {
		return new TreeRangeMultimap<K, V>();
	}

	public static class RangeComparator<C extends Comparable<C>> implements Comparator<Range<C>> {
		public int compare( Range<C> o1, Range<C> o2 ) {
			int c1, c2, c3;

			if (o1.hasLowerBound() && o2.hasLowerBound()) {
				c1 = o1.lowerEndpoint().compareTo( o2.lowerEndpoint() );
				if (c1 < 0) {
					return -1;
				} else if (c1 > 0) {
					return 1;
				}
				c2 = o1.lowerBoundType().compareTo( o2.lowerBoundType() );
				if (c2 != 0) {
					return c2;
				}
			} else if (o1.hasLowerBound() && !o2.hasLowerBound()) {
				return -1;
			} else if (!o1.hasLowerBound() && o2.hasLowerBound()) {
				return 1;
			}

			if (o1.hasUpperBound() && o2.hasUpperBound()) {
				c3 = o1.upperEndpoint().compareTo( o2.upperEndpoint() );
				if (c3 < 0) {
					return -1;
				} else if (c3 > 0) {
					return 1;
				}

				return -o1.upperBoundType().compareTo( o2.upperBoundType() );
			} else if (o1.hasUpperBound() && !o2.hasUpperBound()) {
				return -1;
			} else if (!o1.hasUpperBound() && o2.hasUpperBound()) {
				return 1;
			}

			return 0;
		}
	}

	private Map<Range<K>, Multiset<V>> map = new TreeMap<Range<K>, Multiset<V>>( new RangeComparator<K>() );

	private TreeRangeMultimap() {

	}

	@Override
	@Nullable
	public Multiset<V> get( K key ) {
		Entry<Range<K>, Multiset<V>> e = getEntry( key );
		return e != null ? e.getValue() : null;
	}

	@Override
	@Nullable
	public Entry<Range<K>, Multiset<V>> getEntry( K key ) {
		ImmutableMultiset.Builder<V> b = ImmutableMultiset.builder();
		Range<K> range = null;
		for (Entry<Range<K>, Multiset<V>> e : map.entrySet()) {
			if (e.getKey().contains( key )) {
				if (range == null) {
					range = e.getKey();
				} else {
					range = range.span( e.getKey() );
				}
				b.addAll( e.getValue() );
			}
		}
		return range != null ? new AbstractMap.SimpleEntry<Range<K>, Multiset<V>>( range, b.build() ) : null;

	}

	@Override
	public Range<K> span() {
		Range<K> span = null;
		for (Range<K> r : map.keySet()) {
			if (span == null) {
				span = r;
			} else {
				span = span.span( r );
			}
		}
		return span;
	}

	public void putValue( Range<K> range, V value ) {
		Preconditions.checkNotNull( value );
		if (range.isEmpty()) {
			return;
		}
		for (Entry<Range<K>, Multiset<V>> e : map.entrySet()) {
			if (range.equals( e.getKey() )) {
				e.getValue().add( value );
				return;
			}
		}
		Multiset<V> m = HashMultiset.create();
		m.add( value );
		map.put( range, m );
	}

	@Override
	public void put( Range<K> range, Multiset<V> value ) {
		putAll( range, value );
	}

	public void putAll( Range<K> range, Collection<V> value ) {
		if (range.isEmpty() || value.isEmpty()) {
			return;
		}
		for (Entry<Range<K>, Multiset<V>> e : map.entrySet()) {
			if (range.equals( e.getKey() )) {
				e.getValue().addAll( value );
				return;
			}
		}
		Multiset<V> m = HashMultiset.create();
		m.addAll( value );
		map.put( range, m );
	}

	@Override
	public void putAll( RangeMap<K, Multiset<V>> rangeMap ) {
		for (Entry<Range<K>, Multiset<V>> e : rangeMap.asMapOfRanges().entrySet()) {
			putAll( e.getKey(), e.getValue() );
		}
	}

	@Override
	public void clear() {
		map.clear();
	}

	@Override
	public void remove( Range<K> range ) {
		if (range.isEmpty()) {
			return;
		}
		final Range<K> lower = Range.upTo( range.lowerEndpoint(),
				range.lowerBoundType().compareTo( BoundType.CLOSED ) == 0 ? BoundType.OPEN : BoundType.CLOSED );
		final Range<K> upper = Range.downTo( range.upperEndpoint(),
				range.upperBoundType().compareTo( BoundType.CLOSED ) == 0 ? BoundType.OPEN : BoundType.CLOSED );
		Entry<Range<K>, Multiset<V>> e;
		TreeRangeMultimap<K, V> reAdd = new TreeRangeMultimap<K, V>();
		for (Iterator<Entry<Range<K>, Multiset<V>>> it = map.entrySet().iterator(); it.hasNext();) {
			e = it.next();
			if (range.isConnected( e.getKey() )) {

				it.remove();

				if (lower.isConnected( e.getKey() )) {
					Range<K> inters = lower.intersection( e.getKey() );
					reAdd.put( inters, e.getValue() );
				}

				if (upper.isConnected( e.getKey() )) {
					Range<K> inters = upper.intersection( e.getKey() );
					reAdd.put( inters, e.getValue() );
				}

			}
		}
		if (!reAdd.isEmpty()) {
			this.putAll( reAdd );
		}

	}

	public RangeMap<K, Multiset<V>> removeEnclosed( Range<K> range ) {
		if (range.isEmpty()) {
			return ImmutableRangeMap.of();
		}
		TreeRangeMultimap<K, V> trm = new TreeRangeMultimap<K, V>();
		Entry<Range<K>, Multiset<V>> e;
		for (Iterator<Entry<Range<K>, Multiset<V>>> it = map.entrySet().iterator(); it.hasNext();) {
			e = it.next();
			if (range.isConnected( e.getKey() )) {
				if (range.encloses( e.getKey() )) {
					trm.put( e.getKey(), e.getValue() );
					it.remove();
				}
			}
		}
		return trm;
	}

	public int remove( final V value ) {
		return remove( value, 1 );
	}

	public int remove( final V value, int occurrences ) {
		int result = 0;
		Entry<Range<K>, Multiset<V>> e;
		for (Iterator<Entry<Range<K>, Multiset<V>>> it = map.entrySet().iterator(); it.hasNext() && occurrences > 0;) {
			e = it.next();
			int found = e.getValue().remove( value, occurrences );
			occurrences -= found;
			result += found;
			if (e.getKey().isEmpty() || e.getValue().isEmpty()) {
				it.remove();
			}
		}
		return result;
	}

	@Override
	public Map<Range<K>, Multiset<V>> asMapOfRanges() {
		return ImmutableMap.copyOf( map );
	}

	@Override
	public RangeMap<K, Multiset<V>> subRangeMap( Range<K> range ) {
		TreeRangeMultimap<K, V> trm = new TreeRangeMultimap<K, V>();
		if (!range.isEmpty()) {
			Entry<Range<K>, Multiset<V>> e;
			for (Iterator<Entry<Range<K>, Multiset<V>>> it = map.entrySet().iterator(); it.hasNext();) {
				e = it.next();
				if (range.isConnected( e.getKey() )) {
					Range<K> newR = range.intersection( e.getKey() );
					if (!newR.isEmpty()) {
						trm.put( newR, e.getValue() );
					}
				}
			}
		}
		return trm;
	}

	public RangeMap<K, Multiset<V>> getInclusiveRangeMap( Range<K> range ) {
		TreeRangeMultimap<K, V> trm = new TreeRangeMultimap<K, V>();
		if (!range.isEmpty()) {
			Entry<Range<K>, Multiset<V>> e;
			for (Iterator<Entry<Range<K>, Multiset<V>>> it = map.entrySet().iterator(); it.hasNext();) {
				e = it.next();
				if (range.isConnected( e.getKey() )) {
					trm.put( e.getKey(), e.getValue() );
				}
			}
		}
		return trm;
	}

	public int size() {
		int size = 0;
		for (Multiset<V> m : map.values()) {
			size += m.size();
		}
		return size;
	}

	public boolean isEmpty() {
		return map.isEmpty();
	}

	@SuppressWarnings("unchecked")
	public boolean containsKey( @Nullable Object key ) {
		if (this.isEmpty()) {
			return false;
		}
		if (key instanceof Range) {
			if (map.containsKey( key )) {
				return true;
			}
			try {
				return this.span().isConnected( (Range<K>) key );
			} catch (ClassCastException e) {
				return false;
			}
		} else {
			try {
				return this.get( (K) key ) != null;
			} catch (ClassCastException e) {
				return false;
			}
		}
	}

	public boolean containsValue( @Nullable Object value ) {
		if (this.isEmpty()) {
			return false;
		}
		for (Multiset<V> m : map.values()) {
			if (m.contains( value )) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public boolean containsEntry( @Nullable Object key, @Nullable Object value ) {
		if (this.isEmpty()) {
			return false;
		}
		if (key instanceof Range) {
			try {
				Range<K> r = (Range<K>) key;
				RangeMap<K, Multiset<V>> rm = this.subRangeMap( r );
				for (Entry<Range<K>, Multiset<V>> e : rm.asMapOfRanges().entrySet()) {
					if (e.getValue().contains( value )) {
						return true;
					}
				}
			} catch (ClassCastException e) {
				return false;
			}
		} else {
			try {
				return this.get( (K) key ).contains( value );
			} catch (ClassCastException e) {
				return false;
			}
		}
		return false;
	}

	public boolean put( @Nullable K key, @Nullable V value ) {
		this.putValue( Range.singleton( key ), value );
		return true;
	}

	public boolean putAll( @Nullable K key, Iterable<? extends V> values ) {
		this.putAll( Range.singleton( key ), ImmutableMultiset.copyOf( values ) );
		return values.iterator().hasNext();
	}

	public boolean putAll( Multimap<? extends K, ? extends V> multimap ) {
		for (Entry<? extends K, ? extends V> e : multimap.entries()) {
			this.put( e.getKey(), e.getValue() );
		}
		return multimap.size() > 0;
	}

	public Collection<V> values() {
		ImmutableMultiset.Builder<V> b = ImmutableMultiset.builder();
		for (Multiset<V> m : map.values()) {
			b.addAll( m );
		}
		return b.build();
	}

}
