package de.mickare.LightHoloDisplayAPI.util;

import java.util.logging.Level;

import org.bukkit.plugin.Plugin;

import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.AbstractService;

public abstract class SimpleScheduledService extends AbstractService {
	
	public static abstract class Scheduler {
		
		public static Scheduler timer( final Plugin plugin, final long delay, final long period ) {
			Preconditions.checkNotNull( plugin );
			Preconditions.checkArgument( delay >= 0 );
			Preconditions.checkArgument( period > 0 );
			return new Scheduler() {
				@Override
				public Plugin getPlugin() {
					return plugin;
				}
				@Override
				void schedule( BukkitRunnable2 task ) {
					task.runTaskTimer( plugin, delay, period );
				}
			};
		}
		
		public static Scheduler timerAsynchronously( final Plugin plugin, final long delay, final long period ) {
			Preconditions.checkNotNull( plugin );
			Preconditions.checkArgument( delay >= 0 );
			Preconditions.checkArgument( period > 0 );
			return new Scheduler() {
				@Override
				public Plugin getPlugin() {
					return plugin;
				}
				@Override
				void schedule( BukkitRunnable2 task ) {
					task.runTaskTimerAsynchronously( plugin, delay, period );
				}
			};
		}

		abstract Plugin getPlugin();

		abstract void schedule( BukkitRunnable2 task );
	}
	
	private State state = State.NEW;
	
	public State getState() {
		return state;
	}

	private final BukkitRunnable2 task = new BukkitRunnable2() {
		@Override
		public void run() {
			try {
				SimpleScheduledService.this.runOneIteration();
			} catch ( Exception e ) {
				try {
					SimpleScheduledService.this.notifyFailed( e );
					SimpleScheduledService.this.shutDown();
				} catch ( Exception ex2 ) {
					scheduler().getPlugin().getLogger().log( Level.SEVERE, "Error while shutting down service!", ex2 );
				} finally {
					this.cancel();
				}
			}
		}
	};
	
	protected abstract void runOneIteration() throws Exception;
	
	protected abstract Scheduler scheduler();
	
	/*
	 * Start the service.
	 * 
	 * By default this method does nothing.
	 */
	protected void startUp() throws Exception {
	}
	
	/*
	 * Stop the service. This is guaranteed not to run concurrently with runOneIteration().
	 * 
	 * By default this method does nothing.
	 */
	protected void shutDown() throws Exception {
	}
	
	@Override
	protected void doStart() {
		try {
			this.startUp();
			this.scheduler().schedule( this.task );
			this.notifyStarted();
		} catch ( Throwable t ) {
			this.task.cancel();
			this.notifyFailed( t );
		}
	}
	
	@Override
	protected void doStop() {
		try {
			this.task.cancel();
			this.shutDown();
			this.notifyStopped();
		} catch ( Throwable t ) {
			this.task.cancel();
			this.notifyFailed( t );
		}
	}
}
