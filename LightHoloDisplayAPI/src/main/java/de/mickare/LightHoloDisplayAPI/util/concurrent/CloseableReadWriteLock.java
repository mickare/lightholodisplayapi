package de.mickare.LightHoloDisplayAPI.util.concurrent;

import java.util.concurrent.locks.ReadWriteLock;

public interface CloseableReadWriteLock extends ReadWriteLock {

	@Override
	public CloseableLock readLock();

	@Override
	public CloseableLock writeLock();

}
