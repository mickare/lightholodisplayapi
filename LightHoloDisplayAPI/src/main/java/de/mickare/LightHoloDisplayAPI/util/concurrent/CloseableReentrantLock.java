package de.mickare.LightHoloDisplayAPI.util.concurrent;

import java.util.concurrent.locks.ReentrantLock;

public class CloseableReentrantLock extends ReentrantLock implements AutoCloseable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4139432594795219762L;

	public CloseableReentrantLock open() {
		this.lock();
		return this;
	}
	
	@Override
	public void close() throws Exception {
		this.unlock();
	}

}
