package de.mickare.LightHoloDisplayAPI.animation.position.presets;

import org.apache.commons.math.geometry.Rotation;
import org.apache.commons.math.geometry.Vector3D;

import de.mickare.LightHoloDisplayAPI.animation.position.AbstractOffsetElement;

public class RotationOffset extends AbstractOffsetElement {

	private final Rotation rotation;
	private final Vector3D axis;
	private final double angle;

	public RotationOffset(final Rotation rotation) {
		this.rotation = rotation;
		this.axis = rotation.getAxis();
		this.angle = rotation.getAngle();
	}

	public RotationOffset(final Vector3D axis, final double angle) {
		this.axis = axis;
		this.angle = angle;	
		this.rotation = new Rotation(axis, angle);
	}
	
	@Override
	public Vector3D getOffset( double time ) {
		if(time <= 0) {
			return this.getChildrenOffset( time );
		} else if(time >= 1) {
			return rotation.applyTo( this.getChildrenOffset( time ) );
		}
		return new Rotation(axis, time * angle).applyTo( this.getChildrenOffset( time ) );
	}
	
}
