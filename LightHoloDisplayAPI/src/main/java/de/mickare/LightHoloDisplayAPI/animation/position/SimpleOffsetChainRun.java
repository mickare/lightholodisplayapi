package de.mickare.LightHoloDisplayAPI.animation.position;

import org.apache.commons.math.geometry.Vector3D;

import com.google.common.base.Preconditions;

import de.mickare.LightHoloDisplayAPI.animation.position.presets.DummyOffset;

public class SimpleOffsetChainRun implements OffsetChainRun {

	private final long duration;
	private final OffsetElement root = new DummyOffset();

	public SimpleOffsetChainRun(long duration) {
		Preconditions.checkArgument( duration > 0 );
		this.duration = duration;
	}

	@Override
	public Vector3D getOffset( long start, long time ) {
		if (time < start) {
			return Vector3D.ZERO;
		} else if (time >= start + duration) {
			return root.getOffset( 1 );
		}
		return root.getOffset( ((double) time - (double) start) / (double) duration );
	}

	@Override
	public OffsetElement getRoot() {
		return root;
	}

	@Override
	public long getDuration() {
		return duration;
	}

}
