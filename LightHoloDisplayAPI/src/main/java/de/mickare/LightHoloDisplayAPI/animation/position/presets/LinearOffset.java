package de.mickare.LightHoloDisplayAPI.animation.position.presets;

import org.apache.commons.math.geometry.Vector3D;

import com.google.common.base.Preconditions;

import de.mickare.LightHoloDisplayAPI.animation.EASING;
import de.mickare.LightHoloDisplayAPI.animation.position.AbstractOffsetElement;

public class LinearOffset extends AbstractOffsetElement {

	private final Vector3D end;
	private final EASING easing;

	public LinearOffset(final Vector3D end, final EASING easing) {
		Preconditions.checkNotNull( end );
		Preconditions.checkNotNull( easing );
		this.end = end;
		this.easing = easing;
	}

	@Override
	public Vector3D getOffset( final double time ) {
		if (time >= 1) {
			return end;
		} else if (time <= 0) {
			return this.getChildrenOffset( time );
		}

		return this.getChildrenOffset( time ).add( end.scalarMultiply( easing.apply( time ) ) );
	}

}
