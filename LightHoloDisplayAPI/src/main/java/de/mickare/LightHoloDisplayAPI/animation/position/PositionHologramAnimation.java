package de.mickare.LightHoloDisplayAPI.animation.position;

import java.util.Map.Entry;

import org.apache.commons.math.geometry.Vector3D;
import org.bukkit.util.Vector;

import com.google.common.base.Preconditions;
import com.google.common.collect.BoundType;
import com.google.common.collect.Multiset;
import com.google.common.collect.Range;
import com.google.common.collect.TreeRangeMap;

import de.mickare.LightHoloDisplayAPI.api.IHologram;
import de.mickare.LightHoloDisplayAPI.hologram.HologramLine;
import de.mickare.LightHoloDisplayAPI.util.SimpleScheduledService;
import de.mickare.LightHoloDisplayAPI.util.collect.TreeRangeMultimap;

public class PositionHologramAnimation {
	
	public static class AnimatioService extends SimpleScheduledService {
		private final PositionHologramAnimation animation;
		private final Scheduler scheduler;
		
		private long time = 0;
		private long lastrun;
		
		private volatile boolean paused = false;
		
		private AnimatioService( PositionHologramAnimation animation, Scheduler scheduler ) {
			this.animation = animation;
			this.scheduler = scheduler;
		}
		
		@Override
		protected final void runOneIteration() throws Exception {
			if ( paused ) {
				return;
			}
			if ( animation.hologram.isDestroyed() ) {
				this.stopAsync();
				return;
			}
			final long n = System.currentTimeMillis();
			final long delta = n - lastrun;
			time += delta;
			try {
				animation.updateAndMove( time, delta );
			} catch ( Exception ex ) {
				throw new RuntimeException( ex );
			}
			lastrun = n;
		}
		
		@Override
		protected Scheduler scheduler() {
			return scheduler;
		}
		
		@Override
		protected void startUp() {
			lastrun = System.currentTimeMillis();
		}
		
		public boolean isPaused() {
			return paused;
		}
		
		public void pause() {
			this.paused = true;
		}
		
		public void unpause() {
			if ( !this.paused ) {
				return;
			}
			lastrun = System.currentTimeMillis();
			this.paused = false;
		}
		
	}
	
	private static final double SYNC_CLIENT_WITH_TELEPORT_AFTER_DISTANCE_MOVED = 100;
	private static final double MOVE_PRECISION = 1 / 32 / 2;
	
	private double x, y, z;
	private double offset_x = 0, offset_y = 0, offset_z = 0;
	private double cleaned_offset_x = 0, cleaned_offset_y = 0, cleaned_offset_z = 0;
	private double last_offset_x = 0, last_offset_y = 0, last_offset_z = 0;
	
	private double distance_moved_squared = 0;
	
	private boolean didChangePositionLastTime = false;
	
	private final IHologram hologram;
	
	private final TreeRangeMap<Long, AnimationPosition> positions = TreeRangeMap.create();
	private final TreeRangeMultimap<Long, OffsetChainRun> offsetChains = TreeRangeMultimap.create();
	
	private final AnimatioService animationService;
	
	public PositionHologramAnimation( IHologram hologram, SimpleScheduledService.Scheduler scheduler ) {
		Preconditions.checkNotNull( hologram );
		Preconditions.checkNotNull( scheduler );
		this.hologram = hologram;
		x = hologram.getX();
		y = hologram.getY();
		z = hologram.getZ();
		this.animationService = new AnimatioService( this, scheduler );
	}
	
	public void start() {
		this.animationService.startAsync();
	}
	
	public void pause() {
		this.animationService.pause();
	}
	
	public void unpause() {
		this.animationService.unpause();
	}
	
	public void stop() {
		this.animationService.stopAsync();
	}
	
	private static double difference( double a, double b ) {
		return Math.abs( a - b );
	}
	
	private synchronized void updateAndMove( long time, long delta ) {
		
		// Position
		Vector3D position;
		AnimationPosition positionFunction = positions.get( time );
		if ( positionFunction != null ) {
			position = positionFunction.apply( time, delta );
			this.x = position.getX();
			this.y = position.getY();
			this.z = position.getZ();
		} else {
			position = new Vector3D( x, y, z );
		}
		// Clean positions
		this.positions.remove( Range.atMost( time ) );
		
		double tx, ty, tz;
		// Clean animations
		tx = 0;
		ty = 0;
		tz = 0;
		for ( Entry<Range<Long>, Multiset<OffsetChainRun>> e : this.offsetChains
				.removeEnclosed( Range.upTo( time - delta, BoundType.OPEN ) ).asMapOfRanges().entrySet() ) {
			if ( e.getKey().isEmpty() ) {
				continue;
			}
			final long start = e.getKey().lowerEndpoint();
			for ( OffsetChainRun par : e.getValue() ) {
				Vector3D v = par.getOffset( start, time );
				tx += v.getX();
				ty += v.getY();
				tz += v.getZ();
			}
		}
		this.cleaned_offset_x += tx;
		this.cleaned_offset_y += ty;
		this.cleaned_offset_z += tz;
		
		// Offset
		tx = this.cleaned_offset_x;
		ty = this.cleaned_offset_y;
		tz = this.cleaned_offset_z;
		for ( Entry<Range<Long>, Multiset<OffsetChainRun>> e : this.offsetChains
				.getInclusiveRangeMap( Range.openClosed( time - delta, time ) ).asMapOfRanges().entrySet() ) {
			if ( e.getKey().isEmpty() ) {
				continue;
			}
			final long start = e.getKey().lowerEndpoint();
			for ( OffsetChainRun par : e.getValue() ) {
				Vector3D v = par.getOffset( start, time );
				tx += v.getX();
				ty += v.getY();
				tz += v.getZ();
			}
		}
		this.offset_x = tx;
		this.offset_y = ty;
		this.offset_z = tz;
		
		// Prevent further unecessary movements and ensure that the hologram is on the right spot.
		if ( difference( last_offset_x, offset_x ) < MOVE_PRECISION
				&& difference( last_offset_y, offset_y ) < MOVE_PRECISION
				&& difference( last_offset_z, offset_z ) < MOVE_PRECISION ) {
			if ( didChangePositionLastTime ) {
				didChangePositionLastTime = false;
				this.hologram.teleport( x + offset_x, y + offset_y, z + offset_z );
				distance_moved_squared = 0;
				this.last_offset_x = this.offset_x;
				this.last_offset_y = this.offset_y;
				this.last_offset_z = this.offset_z;
			}
			return;
		}
		
		this.last_offset_x = this.offset_x;
		this.last_offset_y = this.offset_y;
		this.last_offset_z = this.offset_z;
		
		// Change position
		didChangePositionLastTime = true;
		if ( distance_moved_squared >= SYNC_CLIENT_WITH_TELEPORT_AFTER_DISTANCE_MOVED ) {
			this.hologram.teleport( x + offset_x, y + offset_y, z + offset_z );
			distance_moved_squared = 0;
		} else {
			Vector moved = this.hologram.moveOrTeleportTo( x + offset_x, y + offset_y, z + offset_z );
			if ( !HologramLine.canBeMoved( moved ) ) {
				// If the position-change couldn't be done by moving, it was a teleport and we can reset the
				// counter.
				distance_moved_squared = 0;
			} else {
				distance_moved_squared += moved.lengthSquared();
			}
		}
	}
	
	public synchronized void setPosition( double x, double y, double z ) {
		clearPositions();
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public synchronized void setPosition( AnimationPosition position, long start, long end ) {
		this.setPosition( position, Range.closed( start, end ) );
	}
	
	public synchronized void setPosition( AnimationPosition position, Range<Long> intervall ) {
		this.positions.put( intervall, position );
	}
	
	public synchronized void clearPositions() {
		this.positions.clear();
	}
	
	public synchronized final void addOffsetChain( OffsetChainRun chain, long start ) {
		this.offsetChains.putValue( Range.closed( start, start + chain.getDuration() ), chain );
	}
	
	public synchronized final void addOffsetChain( OffsetChainRun chain, Range<Long> intervall ) {
		this.offsetChains.putValue( intervall, chain );
	}
	
	public synchronized final void clearOffsetChain() {
		this.offsetChains.clear();
	}
	
	public IHologram getHologram() {
		return hologram;
	}
}
