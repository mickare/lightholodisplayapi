package de.mickare.LightHoloDisplayAPI.animation.chain;

import org.bukkit.craftbukkit.libs.jline.internal.Preconditions;
import org.bukkit.plugin.Plugin;

import com.google.common.util.concurrent.ListenableFuture;

import de.mickare.LightHoloDisplayAPI.api.IHologram;

public class ChainAnimation {
	
	public static ChainAnimation builder( ChainAnimationElement root ) {
		return new ChainAnimation( root );
	}
	
	private final ChainAnimationElement root;
	private ChainAnimationElement last;
	
	private ChainAnimation( ChainAnimationElement root ) {
		Preconditions.checkNotNull( root );
		this.root = root;
		this.last = root;
	}
	
	public ChainAnimation add( ChainAnimationElement next ) {
		Preconditions.checkNotNull( next );
		this.last.setSuccessor( next );
		this.last = next;
		return this;
	}
	
	public ChainAnimation loop() {
		this.last.setSuccessor( root );
		return this;
	}
	
	public ListenableFuture<?> start( Plugin plugin, IHologram hologram ) {
		return this.root.start( plugin, hologram );
	}
	
}
