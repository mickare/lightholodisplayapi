package de.mickare.LightHoloDisplayAPI.animation.chain;

import java.util.List;

import org.bukkit.craftbukkit.libs.jline.internal.Preconditions;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Lists;
import com.google.common.util.concurrent.AbstractFuture;
import com.google.common.util.concurrent.ListenableFuture;

import de.mickare.LightHoloDisplayAPI.api.IHologram;

public abstract class ChainAnimationElement {
	
	private static class Stack extends AbstractFuture<ChainAnimationElement> {
		
		private boolean interrupted = false;
		private List<BukkitRunnable> tasks = Lists.newArrayList();
		
		private Stack() {
		}
		
		private synchronized boolean addTask( BukkitRunnable task ) {
			if ( interrupted ) {
				return false;
			}
			this.tasks.add( task );
			return true;
		}
		
		@Override
		protected synchronized void interruptTask() {
			interrupted = true;
			this.tasks.forEach( task -> task.cancel() );
		}
		
		private void finish( ChainAnimationElement element ) {
			this.set( element );
		}
		
		private void finish( Exception e ) {
			this.setException( e );
		}
	}
	
	public static abstract class Scheduler {
		
		public static Scheduler direct() {
			return new Scheduler() {
				public void schedule( Plugin plugin, BukkitRunnable task ) {
					task.runTask( plugin );
				}
			};
		}
		
		public static Scheduler later( final long delay ) {
			return new Scheduler() {
				public void schedule( Plugin plugin, BukkitRunnable task ) {
					task.runTaskLater( plugin, delay );
				}
			};
		}
		
		public static Scheduler directAsynchronously() {
			return new Scheduler() {
				public void schedule( Plugin plugin, BukkitRunnable task ) {
					task.runTaskAsynchronously( plugin );
				}
			};
		}
		
		public static Scheduler laterAsynchronously( final long delay ) {
			return new Scheduler() {
				public void schedule( Plugin plugin, BukkitRunnable task ) {
					task.runTaskLaterAsynchronously( plugin, delay );
				}
			};
		}
		
		private Scheduler() {
		}
		
		public abstract void schedule( Plugin plugin, BukkitRunnable task );
		
	}
	
	private ChainAnimationElement successor = null;
	
	private final Scheduler scheduler;
	
	public ChainAnimationElement( Scheduler scheduler ) {
		Preconditions.checkNotNull( scheduler );
		this.scheduler = scheduler;
	}
	
	public ListenableFuture<?> start( final Plugin plugin, final IHologram hologram ) {
		final Stack stack = new Stack();
		this.schedule( plugin, stack, hologram );
		return stack;
	}
	
	public void setSuccessor( ChainAnimationElement successor ) {
		this.successor = successor;
	}
	
	public ChainAnimationElement getSuccessor() {
		return successor;
	}
	
	private void schedule( final Plugin plugin, final Stack stack, final IHologram hologram ) {
		if ( stack.isCancelled() ) {
			return;
		}
		if ( hologram.isDestroyed() ) {
			stack.finish( ChainAnimationElement.this );
			return;
		}
		BukkitRunnable task = new BukkitRunnable() {
			@Override
			public void run() {
				try {
					if ( hologram.isDestroyed() ) {
						stack.finish( ChainAnimationElement.this );
						return;
					}
					handle( hologram );
					final ChainAnimationElement suc = ChainAnimationElement.this.successor;
					if ( suc != null ) {
						suc.schedule( plugin, stack, hologram );
					} else {
						stack.finish( ChainAnimationElement.this );
					}
				} catch ( Exception e ) {
					stack.finish( e );
				}
			}
		};
		if ( !stack.addTask( task ) ) {
			return;
		}
		this.scheduler.schedule( plugin, task );
	}
	
	protected abstract void handle( IHologram hologram );
}
