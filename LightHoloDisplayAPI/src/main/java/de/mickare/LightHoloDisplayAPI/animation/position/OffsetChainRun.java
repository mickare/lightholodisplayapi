package de.mickare.LightHoloDisplayAPI.animation.position;

import org.apache.commons.math.geometry.Vector3D;

public interface OffsetChainRun {

	public Vector3D getOffset(long start, long time);

	public OffsetElement getRoot();
	
	public long getDuration();

	
}
