package de.mickare.LightHoloDisplayAPI.animation.position.presets;


import org.apache.commons.math.geometry.Vector3D;

import de.mickare.LightHoloDisplayAPI.animation.position.AbstractOffsetElement;

public class DummyOffset extends AbstractOffsetElement {


	@Override
	public Vector3D getOffset( double time ) {
		return this.getChildrenOffset( time );
	}

}
