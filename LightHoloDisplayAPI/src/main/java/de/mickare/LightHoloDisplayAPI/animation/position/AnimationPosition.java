package de.mickare.LightHoloDisplayAPI.animation.position;

import java.util.function.BiFunction;

import org.apache.commons.math.geometry.Vector3D;

public interface AnimationPosition extends BiFunction<Long, Long, Vector3D> {
	
	@Override
	public Vector3D apply( Long time, Long delta );
}
