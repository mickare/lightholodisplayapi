package de.mickare.LightHoloDisplayAPI.animation.text.presets;

import java.util.Collections;
import java.util.List;

import de.mickare.LightHoloDisplayAPI.animation.text.TextChainAnimationElement;

public class StaticText extends TextChainAnimationElement {
	
	private List<String> text;
	
	public StaticText( Scheduler scheduler ) {
		this( Collections.emptyList(), scheduler );
	}
	
	public StaticText( List<String> text, Scheduler scheduler ) {
		super( scheduler );
		this.text = text;
	}
	
	@Override
	public boolean modifyText( List<String> text ) {
		text.clear();
		text.addAll( this.text );
		return true;
	}
	
	public List<String> getText() {
		return text;
	}
	
	public void setText( List<String> text ) {
		this.text = text;
	}
	
}
