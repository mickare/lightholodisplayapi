package de.mickare.LightHoloDisplayAPI.animation.position;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.math.geometry.Vector3D;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;

public abstract class AbstractOffsetElement implements OffsetElement {
	
	private final Set<OffsetElement> children = new HashSet<OffsetElement>();
	
	@Override
	public synchronized OffsetElement addChild( OffsetElement child ) {
		Preconditions.checkNotNull( child );
		this.children.add( child );
		return this;
	}
	
	@Override
	public synchronized void clearChildren() {
		this.children.clear();
	}
	
	@Override
	public synchronized boolean hasChildren() {
		return this.children.size() > 0;
	}
	
	@Override
	public synchronized Set<OffsetElement> getChildren() {
		return ImmutableSet.copyOf( this.children );
	}
	
	@Override
	public synchronized Vector3D getChildrenOffset( double time ) {
		Vector3D result = Vector3D.ZERO;
		for ( OffsetElement a : children ) {
			result = result.add( a.getOffset( time ) );
		}
		return result;
	}
}
