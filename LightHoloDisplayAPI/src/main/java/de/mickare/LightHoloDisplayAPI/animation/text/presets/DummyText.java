package de.mickare.LightHoloDisplayAPI.animation.text.presets;

import java.util.List;

import de.mickare.LightHoloDisplayAPI.animation.text.TextChainAnimationElement;

public class DummyText extends TextChainAnimationElement {
	
	public DummyText( Scheduler scheduler ) {
		super( scheduler );
	}
	
	@Override
	public boolean modifyText( List<String> text ) {
		return false;
	}
	
}
