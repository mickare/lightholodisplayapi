package de.mickare.LightHoloDisplayAPI.animation.text;

import java.util.Arrays;
import java.util.List;

import org.bukkit.craftbukkit.libs.jline.internal.Preconditions;
import org.bukkit.plugin.Plugin;

import com.google.common.util.concurrent.ListenableFuture;

import de.mickare.LightHoloDisplayAPI.animation.chain.ChainAnimationElement.Scheduler;
import de.mickare.LightHoloDisplayAPI.animation.text.presets.StaticText;
import de.mickare.LightHoloDisplayAPI.api.IHologram;

public class TextChainAnimation {
	
	public static TextChainAnimation builder( TextChainAnimationElement root ) {
		return new TextChainAnimation( root );
	}
	
	public static TextChainAnimation builder( List<String> text ) {
		return new TextChainAnimation( new StaticText( text, Scheduler.directAsynchronously() ) );
	}
	
	public static TextChainAnimation builder( String... text ) {
		return builder( Arrays.asList( text ) );
	}
	
	private final TextChainAnimationElement root;
	private TextChainAnimationElement last;
	
	private TextChainAnimation( TextChainAnimationElement root ) {
		Preconditions.checkNotNull( root );
		this.root = root;
		this.last = root;
	}
	
	public TextChainAnimation add( TextChainAnimationElement next ) {
		Preconditions.checkNotNull( next );
		this.last.setSuccessor( next );
		this.last = next;
		return this;
	}
	
	public TextChainAnimation text( long delay, String... text ) {
		return this.text( delay, Arrays.asList( text ) );
	}
	
	public TextChainAnimation text( long delay, List<String> text ) {
		return this.add( new StaticText( text, Scheduler.laterAsynchronously( delay ) ) );
	}
	
	public TextChainAnimation loop() {
		this.last.setSuccessor( root );
		return this;
	}
	
	public ListenableFuture<?> start( Plugin plugin, IHologram hologram ) {
		return this.root.start( plugin, hologram );
	}
	
}
