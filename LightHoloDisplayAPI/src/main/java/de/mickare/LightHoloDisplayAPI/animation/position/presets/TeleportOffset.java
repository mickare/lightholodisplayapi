package de.mickare.LightHoloDisplayAPI.animation.position.presets;

import org.apache.commons.math.geometry.Vector3D;

import de.mickare.LightHoloDisplayAPI.animation.position.AbstractOffsetElement;

public class TeleportOffset extends AbstractOffsetElement {

	private final double timePoint;
	private final Vector3D endOffset;

	public TeleportOffset(final double timePoint, final Vector3D endOffset) {
		this.timePoint = timePoint;
		this.endOffset = endOffset;
	}

	@Override
	public Vector3D getOffset( double time ) {
		if(time < timePoint) {
			return this.getChildrenOffset( time );
		}
		return this.getChildrenOffset( time ).add( endOffset );
	}

}
