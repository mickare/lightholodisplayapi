package de.mickare.LightHoloDisplayAPI.animation.position.presets;

import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.math.geometry.Vector3D;

import com.google.common.base.Preconditions;

import de.mickare.LightHoloDisplayAPI.animation.position.AbstractOffsetElement;

public class StaticOffset extends AbstractOffsetElement {

	private final AtomicReference<Vector3D> position = new AtomicReference<Vector3D>( Vector3D.ZERO );

	public StaticOffset() {

	}

	public StaticOffset(Vector3D position) {
		this.setPosition( position );
	}

	@Override
	public Vector3D getOffset( double time ) {
		return getPosition();
	}

	public Vector3D getPosition() {
		return position.get();
	}

	public StaticOffset setPosition( Vector3D position ) {
		Preconditions.checkNotNull( position );
		this.position.set( position );
		return this;
	}

}
