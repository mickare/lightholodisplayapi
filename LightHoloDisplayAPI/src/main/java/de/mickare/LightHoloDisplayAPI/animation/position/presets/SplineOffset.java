package de.mickare.LightHoloDisplayAPI.animation.position.presets;

import java.util.TreeMap;

import org.apache.commons.math.ArgumentOutsideDomainException;
import org.apache.commons.math.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math.analysis.polynomials.PolynomialSplineFunction;
import org.apache.commons.math.geometry.Vector3D;
import org.bukkit.util.Vector;

import com.google.common.base.Preconditions;

import de.mickare.LightHoloDisplayAPI.animation.position.AbstractOffsetElement;

public class SplineOffset extends AbstractOffsetElement {

	private static final SplineInterpolator interpolator = new SplineInterpolator();

	public static final Builder newBuilder( Vector endpoint ) {
		return newBuilder( endpoint.getX(), endpoint.getY(), endpoint.getZ() );
	}

	public static final Builder newBuilder( Vector3D endpoint ) {
		return newBuilder( endpoint.getX(), endpoint.getY(), endpoint.getZ() );
	}

	public static final Builder newBuilder( final double endpoint_x, final double endpoint_y, final double endpoint_z ) {
		return new Builder( endpoint_x, endpoint_y, endpoint_z );
	}

	public static final class PosKeyFrame implements Comparable<PosKeyFrame> {

		public final double time, x, y, z;

		public PosKeyFrame(final double time, final double x, final double y, final double z) {
			Preconditions.checkArgument( time >= 0 && time <= 1, "time must be between 0 and 1!" );
			this.time = time;
			this.x = x;
			this.y = y;
			this.z = z;
		}

		@Override
		public int compareTo( PosKeyFrame o ) {
			return Double.compare( time, o.time );
		}

		public Vector3D toVector() {
			return new Vector3D( x, y, z );
		}

	}

	public static final class Builder {

		private final TreeMap<Double, PosKeyFrame> points = new TreeMap<Double, PosKeyFrame>();
		private final PosKeyFrame endpoint;

		private Builder(final double endX, final double endY, final double endZ) {
			this.endpoint = new PosKeyFrame( 1, endX, endY, endZ );
			clearPoints();
		}

		public final int countPoints() {
			synchronized (points) {
				return this.points.size();
			}
		}

		public final Builder addPoint( final PosKeyFrame point ) {
			synchronized (points) {
				Preconditions.checkNotNull( point );
				this.points.put( point.time, point );
			}
			return this;
		}

		public final Builder clearPoints() {
			synchronized (points) {
				this.points.clear();
				addPoint( new PosKeyFrame( 0, 0, 0, 0 ) );
				addPoint( this.endpoint );
			}
			return this;
		}

		public final SplineOffset build() {
			synchronized (points) {

				if (points.size() < 3) {
					throw new IllegalArgumentException( "Not enough points! At least 3 are necessary." );
				}

				PosKeyFrame end = points.lastEntry().getValue();
				final double[] time = new double[points.size()];
				final double[] x = new double[points.size()];
				final double[] y = new double[points.size()];
				final double[] z = new double[points.size()];

				int i = 0;
				for (final PosKeyFrame pkf : points.values()) {
					time[i] = pkf.time;
					x[i] = pkf.x;
					y[i] = pkf.y;
					z[i] = pkf.z;
					i++;
				}

				final PolynomialSplineFunction spl_x = interpolator.interpolate( time, x );
				final PolynomialSplineFunction spl_y = interpolator.interpolate( time, y );
				final PolynomialSplineFunction spl_z = interpolator.interpolate( time, z );

				return new SplineOffset( new SplineMovementSetting( spl_x, spl_y, spl_z, end ) );
			}
		}

	}

	public static final class SplineMovementSetting {

		public final PolynomialSplineFunction spl_x, spl_y, spl_z;
		public final PosKeyFrame end;

		private SplineMovementSetting(final PolynomialSplineFunction spl_x, final PolynomialSplineFunction spl_y,
				final PolynomialSplineFunction spl_z, PosKeyFrame end) {
			this.spl_x = spl_x;
			this.spl_y = spl_y;
			this.spl_z = spl_z;
			this.end = end;
		}
	}

	private final SplineMovementSetting setting;
	private final Vector3D start, end;

	public SplineOffset(final SplineMovementSetting setting) {
		Preconditions.checkNotNull( setting );
		this.setting = setting;
		try {
			this.start = getVector( 0 );
			this.end = setting.end.toVector();
		} catch (ArgumentOutsideDomainException e) {
			// Should not happen!
			throw new RuntimeException( e );
		}
	}

	public SplineMovementSetting getSplines() {
		return setting;
	}

	private Vector3D getVector( final double process ) throws ArgumentOutsideDomainException {
		return new Vector3D( setting.spl_x.value( process ), setting.spl_y.value( process ), setting.spl_z.value( process ) );
	}

	@Override
	public Vector3D getOffset( final double time ) {
		if (time < 0) {
			return this.getChildrenOffset( time );
		} else if (time == 0) {
			return this.getChildrenOffset( time ).add( start );
		} else if (time >= setting.end.time) {
			return end;
		}

		try {
			return this.getChildrenOffset( time ).add( getVector( time ) );
		} catch (ArgumentOutsideDomainException e) {
			// Should not happen!
			throw new RuntimeException( e );
		}

	}

}
