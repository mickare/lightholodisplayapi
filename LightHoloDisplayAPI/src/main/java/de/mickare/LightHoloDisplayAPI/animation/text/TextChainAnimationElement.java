package de.mickare.LightHoloDisplayAPI.animation.text;

import java.util.List;

import de.mickare.LightHoloDisplayAPI.animation.chain.ChainAnimationElement;
import de.mickare.LightHoloDisplayAPI.api.IHologram;

public abstract class TextChainAnimationElement extends ChainAnimationElement {
	
	public TextChainAnimationElement( Scheduler scheduler ) {
		super( scheduler );
	}
	
	@Override
	public void setSuccessor( ChainAnimationElement successor ) {
		throw new UnsupportedOperationException();
	}
	
	public void setSuccessor( TextChainAnimationElement successor ) {
		super.setSuccessor( successor );
	}
	
	@Override
	public ChainAnimationElement getSuccessor() {
		return ( TextChainAnimationElement ) super.getSuccessor();
	}
	
	@Override
	protected void handle( IHologram hologram ) {
		List<String> text = hologram.getText();
		if ( this.modifyText( text ) ) {
			hologram.setText( text );
		}
	}
	
	/**
	 * Modify the text of a hologram
	 * 
	 * @param text
	 *            , list to be modifed
	 * @return true if the text was modified and the hologram should be updated
	 */
	public abstract boolean modifyText( List<String> text );
	
}
