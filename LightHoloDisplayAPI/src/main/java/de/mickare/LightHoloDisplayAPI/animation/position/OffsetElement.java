package de.mickare.LightHoloDisplayAPI.animation.position;

import java.util.Set;

import org.apache.commons.math.geometry.Vector3D;

public interface OffsetElement {


	/**
	 * Calculates the animations process from 0 to 1
	 * @param time 0 to 1; 0 is start; 1 is end
	 * @return Offset vector from zero
	 */
	public Vector3D getOffset( double time );
	
	public OffsetElement addChild( OffsetElement child );

	public void clearChildren();

	public boolean hasChildren();

	public Set<OffsetElement> getChildren();

	/**
	 * Calculates the childrens process from 0 to 1
	 * 
	 * @param time
	 *            0 to 1; 0 is start; 1 is end
	 * @return Offset vector from zero
	 */
	public Vector3D getChildrenOffset( double time );
}
