package de.mickare.LightHoloDisplayAPI;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;

import de.mickare.LightHoloDisplayAPI.hologram.HologramManager;
import de.mickare.LightHoloDisplayAPI.listener.PlayerListener;

public class LHD_Plugin extends JavaPlugin {
	
	private ProtocolManager pManager;
	
	private HologramManager holomanager;
	
	private boolean debugging = true;
	
	private double verticalLineSpacing = 0.25;
	private double armorstand_vertical_offset = -2;
	private boolean armorstand_small = true;
	
	@Override
	public void onEnable() {
		this.saveDefaultConfig();
		reloadHologramConfig();
		
		LHDAPI.initialize( this );
		this.pManager = ProtocolLibrary.getProtocolManager();
		this.holomanager = new HologramManager();
		getLogger().info( "Using entityID generator for " + this.holomanager.getVersion().name() );
		
		Bukkit.getPluginManager().registerEvents( new PlayerListener( this ), this );
		
	}
	
	@Override
	public void onDisable() {
		
	}
	
	public void reloadHologramConfig() {
		
		this.armorstand_vertical_offset = this.getConfig().getDouble( "Hologram.armorstand.verticalOffset", -2 );
		this.armorstand_small = this.getConfig().getBoolean( "Hologram.armorstand.small", true );
		
		this.debugging = this.getConfig().getBoolean( "Hologram.debugging", true );
		
	}
	
	public ProtocolManager getProtocolManager() {
		return this.pManager;
	}
	
	public HologramManager getHologramManager() {
		return holomanager;
	}
	
	public void debug( String msg ) {
		if ( debugging ) {
			getLogger().info( msg );
		}
	}
	
	public double getDefaultArmorStandVerticalOffset() {
		return armorstand_vertical_offset;
	}
	
	public boolean isDefaultArmorStandSmall() {
		return armorstand_small;
	}
	
	public double getDefaultVerticalLineSpacing() {
		return verticalLineSpacing;
	}
	
}
