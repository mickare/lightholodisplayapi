package de.mickare.LightHoloDisplayAPI.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import de.mickare.LightHoloDisplayAPI.LHD_Plugin;

public class PlayerListener implements Listener {

	private final LHD_Plugin plugin;

	public PlayerListener(LHD_Plugin plugin) {
		this.plugin = plugin;
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
	public void onPlayerDeath( PlayerRespawnEvent event ) {
		this.plugin.getHologramManager().updatePlayer( event.getPlayer() );
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
	public void onPlayerDeath( PlayerDeathEvent event ) {
		this.plugin.getHologramManager().updatePlayer( event.getEntity() );
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerMove( PlayerMoveEvent event ) {
		if (event.getFrom().getWorld() == event.getTo().getWorld() && event.getFrom().getBlockX() == event.getTo().getBlockX()
				&& event.getFrom().getBlockY() == event.getTo().getBlockY() && event.getFrom().getBlockZ() == event.getTo().getBlockZ())
			return;

		this.plugin.getHologramManager().updatePlayer( event.getPlayer() );

	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
	public void onPlayerChangedWorld( PlayerChangedWorldEvent event ) {
		this.plugin.getHologramManager().updatePlayer( event.getPlayer() );
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
	public void onPlayerQuit( PlayerQuitEvent event ) {
		this.plugin.getHologramManager().removeFromPlayer( event.getPlayer() );
	}

}
