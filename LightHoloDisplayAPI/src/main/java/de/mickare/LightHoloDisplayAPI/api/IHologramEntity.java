package de.mickare.LightHoloDisplayAPI.api;

import java.util.stream.Stream;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import de.mickare.LightHoloDisplayAPI.hologram.wrapper.AbstractEntityPacketWrapper;

public interface IHologramEntity {
	
	void setText( String text );
	
	String getText();
	
	AbstractEntityPacketWrapper getEntityWrapper();
	
	void sendSpawnPacket( Stream<Player> players, Location loc );
	
	void sendSpawnPacket( Stream<Player> players, double x, double y, double z );
	
	void sendDestroyPacket( Stream<Player> players );
	
	void sendTeleportPacket( Stream<Player> players, double x, double y, double z );
	
	void sendTeleportPacket( Stream<Player> players, Location loc );
	
	void sendMetadataPacket( Stream<Player> players );
	
	void sendRelMovePacket( Stream<Player> players, double dx, double dy, double dz );
	
	void sendVelocityPacket( Stream<Player> players, double vx, double vy, double vz );
	
	int[] getEntityIds();
	
}
