package de.mickare.LightHoloDisplayAPI.api;

import org.bukkit.Location;
import org.bukkit.World;

public interface Teleportable {

	public void teleport( Location loc );

	public void teleport( World world, double x, double y, double z, float pitch, float yaw );

	public void teleport( double x, double y, double z );

	public void teleport( double x, double y, double z, float pitch, float yaw );
}
