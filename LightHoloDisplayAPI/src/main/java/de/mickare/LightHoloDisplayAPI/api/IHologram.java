package de.mickare.LightHoloDisplayAPI.api;

import java.util.Set;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

public interface IHologram extends Moveable, Teleportable, TextContent, Locator {

	boolean hasLocation( Location loc );

	boolean hasLocation( World world, double x, double y, double z );

	void teleport( World world, double locX, double locY, double locZ );

	boolean isInSight( Player player );

	void updatePlayer( Player player );

	void addToPlayer( Player player );

	void removeFromPlayer( Player player );

	void destroy();

	boolean isDestroyed();

	double getLineHeight();

	void setLineHeight( double verticalLineSpacing );

	int getSize();

	Set<Player> getPlayers();

	int countPlayers();

	double getX();

	double getY();

	double getZ();

}