package de.mickare.LightHoloDisplayAPI.api;

import java.util.Collection;

import org.bukkit.entity.Player;

import de.mickare.LightHoloDisplayAPI.hologram.HologramManager;

public interface IHologramLine {
	
	String getText();
	
	void setText( String text );
	
	void destroy( HologramManager manager );
	
	void teleport( double locX, double locY, double locZ );
	
	void move( double dx, double dy, double dz );
	
	void spawnTo( Player player );
	
	void spawnToAll( Collection<Player> players );
	
	void despawnTo( Player player );
	
	void despawnToAll();
	
	void updateText();
	
	void moveOrTeleportTo( double locX, double locY, double locZ );
	
	void moveOrTeleportDelta( double dx, double dy, double dz );
	
	@Deprecated
	void velocity( double vx, double vy, double vz );
	
}