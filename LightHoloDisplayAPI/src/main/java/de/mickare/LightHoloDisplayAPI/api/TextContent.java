package de.mickare.LightHoloDisplayAPI.api;

import java.util.List;

public interface TextContent {
	
	public List<String> getText();
	
	public void setText( String... text );
	
	public void setText( List<String> text );
	
}
