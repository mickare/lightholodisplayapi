package de.mickare.LightHoloDisplayAPI.api;

import org.bukkit.util.Vector;

public interface Moveable {
	
	public Vector move( double dx, double dy, double dz );
	
	public Vector moveOrTeleportDelta( double dx, double dy, double dz );
	
	@Deprecated
	public void velocity( double vx, double vy, double vz );
	
	public Vector moveOrTeleportTo( double x, double y, double z );
	
}
