package de.mickare.LightHoloDisplayAPI.api;

import org.bukkit.Location;
import org.bukkit.World;

public interface Locator {
	
	public Location getLocation();

	public World getWorld();
	
}
