package de.mickare.LightHoloDisplayAPI.api;

import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.google.common.base.Preconditions;

public abstract class ForwardingHologram implements IHologram {
	
	private final IHologram handle;
	
	public ForwardingHologram( final IHologram handle ) {
		Preconditions.checkNotNull( handle );
		this.handle = handle;
	}
	
	@Override
	public double getX() {
		return handle.getX();
	}

	@Override
	public double getY() {
		return handle.getY();
	}

	@Override
	public double getZ() {
		return handle.getZ();
	}
	
	@Override
	public void teleport( final Location loc ) {
		handle.teleport( loc );
	}
	
	@Override
	public void teleport( double x, double y, double z ) {
		handle.teleport( x, y, z );
	}
	
	@Override
	public void teleport( double x, double y, double z, float pitch, float yaw ) {
		handle.teleport( x, y, z, pitch, yaw );
	}
	
	@Override
	public void teleport( final World world, final double x, final double y, final double z, final float pitch,
			final float yaw ) {
		handle.teleport( world, x, y, z, pitch, yaw );
	}
	
	@Override
	public void teleport( final World world, final double locX, final double locY, final double locZ ) {
		handle.teleport( world, locX, locY, locZ );
	}
	
	@Override
	public Vector move( final double dx, final double dy, final double dz ) {
		return handle.move( dx, dy, dz );
	}
	
	@Override
	public Vector moveOrTeleportDelta( double dx, double dy, double dz ) {
		return handle.moveOrTeleportDelta( dx, dy, dz );
	}
	
	@Override
	public Vector moveOrTeleportTo( double x, double y, double z ) {
		return handle.moveOrTeleportTo( x, y, z );
	}
	
	@Override
	@Deprecated
	public void velocity( final double vx, final double vy, final double vz ) {
		handle.velocity( vx, vy, vz );
	}
	
	@Override
	public Location getLocation() {
		return handle.getLocation();
	}
	
	@Override
	public World getWorld() {
		return handle.getWorld();
	}
	
	@Override
	public List<String> getText() {
		return handle.getText();
	}
	
	@Override
	public void setText( final String... text ) {
		handle.setText( text );
	}
	
	@Override
	public void setText( final List<String> text ) {
		handle.setText( text );
	}
	
	@Override
	public boolean hasLocation( final Location loc ) {
		return handle.hasLocation( loc );
	}
	
	@Override
	public boolean hasLocation( final World world, final double x, final double y, final double z ) {
		return handle.hasLocation( world, x, y, z );
	}
	
	@Override
	public boolean isInSight( final Player player ) {
		return handle.isInSight( player );
	}
	
	@Override
	public void updatePlayer( final Player player ) {
		handle.updatePlayer( player );
	}
	
	@Override
	public void addToPlayer( final Player player ) {
		handle.addToPlayer( player );
	}
	
	@Override
	public void removeFromPlayer( final Player player ) {
		handle.removeFromPlayer( player );
	}
	
	@Override
	public void destroy() {
		handle.destroy();
	}
	
	@Override
	public boolean isDestroyed() {
		return handle.isDestroyed();
	}
	
	@Override
	public double getLineHeight() {
		return handle.getLineHeight();
	}
	
	@Override
	public void setLineHeight( final double verticalLineSpacing ) {
		handle.setLineHeight( verticalLineSpacing );
	}
	
	@Override
	public int getSize() {
		return handle.getSize();
	}
	
	public IHologram getHandle() {
		return handle;
	}
	
	@Override
	public Set<Player> getPlayers() {
		return handle.getPlayers();
	}
	
	@Override
	public int countPlayers() {
		return handle.countPlayers();
	}
	
}
