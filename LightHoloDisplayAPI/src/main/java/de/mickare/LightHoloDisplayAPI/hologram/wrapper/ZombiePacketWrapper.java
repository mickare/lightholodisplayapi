package de.mickare.LightHoloDisplayAPI.hologram.wrapper;

import org.bukkit.entity.EntityType;

import com.comphenix.protocol.wrappers.WrappedDataWatcher;

public class ZombiePacketWrapper extends LivingEntityPacketWrapper {

    private static WrappedDataWatcher watcherCache = null;

    public static final WrappedDataWatcher getNewWatcher() {
        if (watcherCache != null) {
            return watcherCache.deepClone();
        }
        // final World w = Bukkit.getWorlds().get( 0 );
        // final Entity e = w.spawnEntity( w.getSpawnLocation().clone().add( 0,
        // 255, 0 ), EntityType.HORSE );
        // watcherCache = WrappedDataWatcher.getEntityWatcher( e ).deepClone();
        // e.remove();

        watcherCache = new WrappedDataWatcher();
        // Entity
        watcherCache.setObject(0, (byte) 0); // fire/crouched/...
        watcherCache.setObject(1, (short) 10); // air
        // Living Entity
        watcherCache.setObject(2, "Zombie"); // custom name
        watcherCache.setObject(3, (byte) 0); // use custom

        watcherCache.setObject(6, (float) 20); // health
        watcherCache.setObject(7, (int) 0); // potion
        watcherCache.setObject(8, (byte) 0); //potion ambient
        watcherCache.setObject(9, (byte) 0); // arrows in entity
        watcherCache.setObject(15, (byte) 0); // no ai
        // Horse
        watcherCache.setObject(12, (byte) 0); // child
        watcherCache.setObject(13, (byte) 0); // villager
        watcherCache.setObject(14, (byte) 0); // converting

        return watcherCache.deepClone();
    }

    public ZombiePacketWrapper(int entityId, int typeId, WrappedDataWatcher watcher) {
        super(entityId, typeId, watcher);
        // TODO Auto-generated constructor stub
    }

    private boolean child = false;
    private boolean villager = false;
    private boolean converting = false; 

    public ZombiePacketWrapper(final int entityId) {
        this(entityId, getNewWatcher());
    }

    public ZombiePacketWrapper(final int entityId, final WrappedDataWatcher watcher) {
        super(entityId, EntityType.ZOMBIE, watcher);
    }

    public boolean isChild() {
        return child;
    }

    public boolean isVillager() {
        return villager;
    }

    public boolean isConverting() {
        return converting;
    }

    public void setChild(boolean child) {
        this.child = child;
        this.getDefaultWatcher().setObject(12, (byte)(child ? 0 : 1));
    }

    public void setVillager(boolean villager) {
        this.villager = villager;
        this.getDefaultWatcher().setObject(13, (byte)(villager ? 0 : 1));
    }

    public void setConverting(boolean converting) {
        this.converting = converting;
        this.getDefaultWatcher().setObject(14, (byte)(converting ? 0 : 1));
    }
    
    

}
