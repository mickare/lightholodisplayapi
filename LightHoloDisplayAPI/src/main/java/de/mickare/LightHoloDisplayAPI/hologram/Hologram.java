package de.mickare.LightHoloDisplayAPI.hologram;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.libs.jline.internal.Preconditions;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.google.common.collect.ImmutableSet;

import de.mickare.LightHoloDisplayAPI.LHDAPI;
import de.mickare.LightHoloDisplayAPI.api.IHologram;
import de.mickare.LightHoloDisplayAPI.util.concurrent.CloseableLock;
import de.mickare.LightHoloDisplayAPI.util.concurrent.CloseableReentrantReadWriteLock;

public class Hologram implements IHologram {
	
	public static final int SEE_RADIUS = 50;
	public static final int SEE_RADIUS_SQUARED = SEE_RADIUS * SEE_RADIUS;
	
	private static enum PLAYER_SIGHT {
		SIGHT_STAYED_IN,
		SIGHT_ADDED,
		SIGHT_REMOVED,
		SIGHT_STAYED_OUT;
	}
	
	public static enum BOUND {
		TOP( new OffsetFunction() {
			@Override
			public double getOffset( int index, int size, double offset ) {
				return -( index + 1 ) * offset;
			}
		} ),
		MIDDLE( new OffsetFunction() {
			@Override
			public double getOffset( int index, int size, double offset ) {
				return ( size / 2 ) * offset - index * offset;
			}
		} ),
		BOTTOM( new OffsetFunction() {
			@Override
			public double getOffset( int index, int size, double offset ) {
				return ( size - 1 ) * offset - index * offset;
			}
		} );
		
		private final OffsetFunction offset;
		
		private BOUND( OffsetFunction offset ) {
			this.offset = offset;
		}
		
		public double getOffset( int index, int size, double offset ) {
			return this.offset.getOffset( index, size, offset );
		}
		
		public static interface OffsetFunction {
			public double getOffset( int index, int size, double offset );
		}
	}
	
	public static final Builder newBuilder( Location location ) {
		return new Builder( location );
	}
	
	public static final class Builder {
		
		private Location location = null;
		private double lineHeight = LHDAPI.getDefaultLineHeight();
		private BOUND bounding = BOUND.BOTTOM;
		
		public Builder( Location location ) {
			this.location = location;
		}
		
		public Hologram build() {
			Preconditions.checkNotNull( location );
			Hologram holo = new Hologram( LHDAPI.getManager(), location, bounding );
			holo.setLineHeight( this.lineHeight );
			return holo;
		}
		
		public double getLineHeight() {
			return lineHeight;
		}
		
		public Builder setLineHeight( double lineHeight ) {
			this.lineHeight = lineHeight;
			return this;
		}
		
		public BOUND getBounding() {
			return bounding;
		}
		
		public Builder setBounding( BOUND bounding ) {
			this.bounding = bounding;
			return this;
		}
	}
	
	private final BOUND bounding;
	
	private final HologramManager manager;
	
	private volatile double locX, locY, locZ;
	private World world;
	
	private boolean destroyed = false;
	
	private double lineHeight = 0.25;
	
	private final LoadingCache<Integer, HologramLine> cachedLines = CacheBuilder.newBuilder()
			.removalListener( new RemovalListener<Integer, HologramLine>() {
				@Override
				public void onRemoval( RemovalNotification<Integer, HologramLine> n ) {
					n.getValue().destroy( Hologram.this.manager );
				}
			} ).build( new CacheLoader<Integer, HologramLine>() {
				@Override
				public HologramLine load( Integer index ) throws Exception {
					HologramLine line = new HologramLine( manager.getNextFreeId( Hologram.this.getLocation() ),
							Hologram.this.locX, Hologram.this.locY, Hologram.this.locZ );
					line.setOffsetY( Hologram.this.bounding.getOffset( index, ( int ) cachedLines.size(), Hologram.this.lineHeight ) );
					return line;
				}
			} );
	
	private final ArrayList<HologramLine> lines = new ArrayList<HologramLine>();
	private final Set<Player> players = Collections.newSetFromMap( new WeakHashMap<Player, Boolean>() );
	private final Set<Player> seePlayers = Collections.newSetFromMap( new WeakHashMap<Player, Boolean>() );
	
	private final CloseableReentrantReadWriteLock hololock = new CloseableReentrantReadWriteLock();
	
	private Hologram( HologramManager manager, Location location, BOUND bounding ) {
		Preconditions.checkNotNull( manager );
		Preconditions.checkNotNull( bounding );
		Preconditions.checkNotNull( location );
		this.manager = manager;
		this.world = location.getWorld();
		this.locX = location.getX();
		this.locY = location.getY();
		this.locZ = location.getZ();
		this.bounding = bounding;
		
		manager.registerHologram( this );
	}
	
	@Override
	public double getX() {
		return locX;
	}
	
	@Override
	public double getY() {
		return locY;
	}
	
	@Override
	public double getZ() {
		return locZ;
	}
	
	private void checkState() {
		if ( isDestroyed() ) {
			throw new IllegalStateException( "This hologram was destroyed! You should not use it again!" );
		}
	}
	
	private final PLAYER_SIGHT _updatePlayer( final Player player ) {
		try ( CloseableLock cl = hololock.readLock().open() ) {
			checkState();
			if ( this.isInSight( player ) ) {
				lines.forEach( line -> line.spawnTo( player ) );
				if ( this.seePlayers.add( player ) ) {
					return PLAYER_SIGHT.SIGHT_ADDED;
				} else {
					return PLAYER_SIGHT.SIGHT_STAYED_IN;
				}
			} else {
				lines.forEach( line -> line.despawnTo( player ) );
				if ( this.seePlayers.remove( player ) ) {
					return PLAYER_SIGHT.SIGHT_REMOVED;
				} else {
					return PLAYER_SIGHT.SIGHT_STAYED_OUT;
				}
			}
		}
	}
		
	@Override
	public Vector move( final double dx, final double dy, final double dz ) {
		if ( !HologramLine.canBeMoved( dx, dy, dz ) ) {
			throw new IllegalArgumentException( "Displacement cannot exceed 4 meters." );
		}
		try ( CloseableLock cl = hololock.writeLock().open() ) {
			checkState();
			final double tdx = HologramLine.translateMovePrecision( dx );
			final double tdy = HologramLine.translateMovePrecision( dy );
			final double tdz = HologramLine.translateMovePrecision( dz );
			if ( tdx == 0 && tdy == 0 && tdz == 0 ) {
				return new Vector( 0, 0, 0 );
			}
			this.locX += tdx;
			this.locY += tdy;
			this.locZ += tdz;
			
			players.forEach( this::_updatePlayer );
			this.lines.forEach( line -> line.move( tdx, tdy, tdz ) );
			
			return new Vector( tdx, tdy, tdz );
		}
	}
	
	@Override
	public Vector moveOrTeleportDelta( final double dx, final double dy, final double dz ) {
		if ( HologramLine.canBeMoved( dx, dy, dz ) ) {
			return move( dx, dy, dz );
		} else {
			teleport( this.locX + dx, this.locY + dy, this.locZ + dz );
			return new Vector( dx, dy, dz );
		}
	}
	
	@Override
	public Vector moveOrTeleportTo( final double x, final double y, final double z ) {
		double dx = x - this.locX, dy = y - this.locY, dz = z - this.locZ;
		if ( HologramLine.canBeMoved( dx, dy, dz ) ) {
			return move( dx, dy, dz );
		} else {
			teleport( x, y, z );
			return new Vector( dx, dy, dz );
		}
	}
	
	@Override
	@Deprecated
	public void velocity( final double vx, final double vy, final double vz ) {
		try ( CloseableLock cl = hololock.writeLock().open() ) {
			checkState();
			this.locX += HologramLine.velocityToBlock( vx );
			this.locY += HologramLine.velocityToBlock( vy );
			this.locZ += HologramLine.velocityToBlock( vz );
			
			this.players.forEach( this::_updatePlayer );
			this.lines.forEach( line -> line.velocity( vx, vy, vz ) );
		}
	}
	
	@Override
	public void teleport( final Location loc ) {
		this.teleport( loc.getWorld(), loc.getX(), loc.getY(), loc.getZ(), loc.getPitch(), loc.getYaw() );
	}
	
	@Override
	public void teleport( final World world, final double x, final double y, final double z, final float pitch,
			final float yaw ) {
		Preconditions.checkNotNull( world );
		try ( CloseableLock cl = hololock.writeLock().open() ) {
			checkState();
			this.world = world;
			this.teleport( x, y, z, pitch, yaw );
		}
	}
	
	@Override
	public void teleport( final World world, final double x, final double y, final double z ) {
		this.teleport( world, x, y, z, 0, 0 );
	}
	
	@Override
	public void teleport( final double x, final double y, final double z, final float pitch, final float yaw ) {
		this.teleport( x, y, z );
	}
	
	@Override
	public void teleport( final double x, final double y, final double z ) {
		try ( final CloseableLock cl = hololock.writeLock().open() ) {
			checkState();
			this.locX = x;
			this.locY = y;
			this.locZ = z;
			this.players.forEach( this::_updatePlayer );
			this.lines.forEach( line -> line.teleport( x, y, z ) );
		}
	}
	
	@Override
	public List<String> getText() {
		try ( final CloseableLock cl = this.hololock.readLock().open() ) {
			final List<String> text = new ArrayList<String>( this.lines.size() );
			this.lines.forEach( line -> text.add( line.getText() ) );
			return text;
		}
	}
	
	@Override
	public void setText( final String... text ) {
		this.setText( Arrays.asList( text ) );
	}
	
	@Override
	public void setText( final List<String> text ) {
		final int textSize = text.size();
		try ( final CloseableLock cl = this.hololock.writeLock().open() ) {
			if ( textSize == 0 ) {
				this.lines.forEach( line -> line.despawnToAll() );
				this.lines.clear();
				return;
			}
			// Remove not needed lines
			while ( this.lines.size() > textSize ) {
				this.lines.remove( this.lines.size() - 1 ).despawnToAll();
			}
			// Add needed lines
			while ( this.lines.size() < textSize ) {
				this.lines.add( cachedLines.getUnchecked( this.lines.size() ) );
			}
			// Set text of lines
			for ( int index = 0; index < textSize; index++ ) {
				HologramLine line = this.lines.get( index );
				line.setText( text.get( index ) );
				line.setOffsetY( this.bounding.getOffset( index, textSize, this.lineHeight ) );
			}
			// Move to the correct place, because the line positions could have changed
			this.lines.forEach( line -> line.teleport( this.locX, this.locY, this.locZ ) );
			// Spawn if necessary
			this.lines.forEach( line -> line.spawnToAll( this.seePlayers ) );
			// Send update
			this.lines.forEach( line -> line.updateText() );
		}
	}
	
	@Override
	public Location getLocation() {
		try ( final CloseableLock cl = this.hololock.readLock().open() ) {
			return new Location( this.world, this.locX, this.locY, this.locZ );
		}
	}
	
	@Override
	public World getWorld() {
		try ( final CloseableLock cl = this.hololock.readLock().open() ) {
			return this.world;
		}
	}
	
	@Override
	public boolean hasLocation( final Location loc ) {
		return hasLocation( loc.getWorld(), loc.getX(), loc.getY(), loc.getZ() );
	}
	
	@Override
	public boolean hasLocation( final World world, final double x, final double y, final double z ) {
		try ( final CloseableLock cl = hololock.readLock().open() ) {
			this.checkState();
			return this.world.equals( world ) && x == this.locX && y == this.locY && z == this.locZ;
		}
	}
	
	private final double distanceSqrd( final Location l ) {
		double x = this.locX - l.getX(), y = this.locY - l.getY(), z = this.locZ - l.getZ();
		return x * x + y * y + z * z;
	}
	
	@Override
	public boolean isInSight( final Player player ) {
		try ( final CloseableLock cl = hololock.readLock().open() ) {
			this.checkState();
			if ( player.getWorld() == this.getWorld() && distanceSqrd( player.getLocation() ) <= SEE_RADIUS_SQUARED ) {
				return true;
			}
			return false;
		}
	}
	
	@Override
	public void updatePlayer( final Player player ) {
		Preconditions.checkNotNull( player );
		try ( final CloseableLock cl = hololock.readLock().open() ) {
			this.checkState();
			if ( !players.contains( player ) ) {
				return;
			}
			this._updatePlayer( player );
		}
	}
	
	@Override
	public void addToPlayer( final Player player ) {
		Preconditions.checkNotNull( player );
		try ( final CloseableLock cl = hololock.writeLock().open() ) {
			this.players.add( player );
			this._updatePlayer( player );
		}
	}
	
	@Override
	public void removeFromPlayer( final Player player ) {
		Preconditions.checkNotNull( player );
		try ( final CloseableLock cl = hololock.writeLock().open() ) {
			this.players.remove( player );
			this.seePlayers.remove( player );
			this.lines.forEach( line -> line.despawnTo( player ) );
		}
	}
	
	@Override
	public void destroy() {
		try ( final CloseableLock cl = hololock.writeLock().open() ) {
			if ( this.destroyed ) {
				return;
			}
			this.destroyed = true;
			this.manager.unregisterHologram( this );
			this.players.clear();
			this.seePlayers.clear();
			this.lines.forEach( line -> line.destroy( this.manager ) );
			this.lines.clear();
			this.world = null;
		}
	}
	
	@Override
	public boolean isDestroyed() {
		try ( final CloseableLock cl = hololock.readLock().open() ) {
			return this.destroyed;
		}
	}
	
	@Override
	public double getLineHeight() {
		try ( final CloseableLock cl = hololock.readLock().open() ) {
			return this.lineHeight;
		}
	}
	
	@Override
	public void setLineHeight( final double lineHeight ) {
		try ( final CloseableLock cl = hololock.writeLock().open() ) {
			this.lineHeight = lineHeight;
		}
	}
	
	@Override
	public int getSize() {
		try ( final CloseableLock cl = hololock.readLock().open() ) {
			return this.lines.size();
		}
	}
	
	@Override
	public Set<Player> getPlayers() {
		try ( final CloseableLock cl = hololock.readLock().open() ) {
			return ImmutableSet.copyOf( this.players );
		}
	}
	
	@Override
	public int countPlayers() {
		try ( final CloseableLock cl = hololock.readLock().open() ) {
			return this.players.size();
		}
	}
	
}
