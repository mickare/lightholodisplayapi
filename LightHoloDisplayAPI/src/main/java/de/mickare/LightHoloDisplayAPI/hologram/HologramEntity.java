package de.mickare.LightHoloDisplayAPI.hologram;

import java.util.stream.Stream;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.comphenix.packetwrapper.AbstractPacket;

import de.mickare.LightHoloDisplayAPI.LHDAPI;
import de.mickare.LightHoloDisplayAPI.api.IHologramEntity;
import de.mickare.LightHoloDisplayAPI.hologram.wrapper.ArmorStandPacketWrapper;

public abstract class HologramEntity implements IHologramEntity {
	
	public static final int MAX_NAME_LENGTH = 300;
	
	private String text = null;
	
	public static class ArmorStandEntity extends HologramEntity {
		
		private final ArmorStandPacketWrapper armorstand;
		
		private final double verticalOffset = LHDAPI.getDefaultArmorStandVerticalOffset();
		
		public ArmorStandEntity( final int armorstandId ) {
			super();
			this.armorstand = new ArmorStandPacketWrapper( armorstandId );
			this.armorstand.setHasGravity( false );
			this.armorstand.setInvisible( true );
			this.armorstand.setSmallArmorStand( LHDAPI.isDefaultArmorStandSmall() );
		}
		
		public ArmorStandPacketWrapper getArmorStandWrapper() {
			return armorstand;
		}
		
		@Override
		public ArmorStandPacketWrapper getEntityWrapper() {
			return this.armorstand;
		}
		
		@Override
		public final void sendSpawnPacket( final Stream<Player> players, final Location loc ) {
			final AbstractPacket packet = armorstand.createSpawnPacket( loc.add( 0, verticalOffset, 0 ) );
			players.forEach( player -> packet.sendPacket( player ) );
		}
		
		@Override
		public final void sendSpawnPacket( final Stream<Player> players, final double x, double y, double z ) {
			final AbstractPacket packet = armorstand.createSpawnPacket( x, y + verticalOffset, z, 0, 0 );
			players.forEach( player -> packet.sendPacket( player ) );
		}

		@Override
		public final void sendDestroyPacket( final Stream<Player> players ) {
			final AbstractPacket packet = armorstand.createDestroyPacket();
			players.forEach( player -> packet.sendPacket( player ) );
		}

		@Override
		public final int[] getEntityIds() {
			return new int[] { this.armorstand.getEntityId() };
		}
		
		@Override
		public final void sendTeleportPacket( final Stream<Player> players, final double x, final double y,
				final double z ) {
			final AbstractPacket packet = this.armorstand.createTeleportPacket( x, y + verticalOffset, z, 0, 0 );
			players.forEach( player -> packet.sendPacket( player ) );
		}
		
		@Override
		public final void sendTeleportPacket( final Stream<Player> players, final Location loc ) {
			this.sendTeleportPacket( players, loc.getX(), loc.getY() + verticalOffset, loc.getZ() );
		}
		
		@Override
		public final void sendMetadataPacket( final Stream<Player> players ) {
			final AbstractPacket packet = this.armorstand.createMetaPacket();
			players.forEach( player -> packet.sendPacket( player ) );
		}
		
		@Override
		public final void sendRelMovePacket( final Stream<Player> players, final double dx, final double dy,
				final double dz ) {
			final AbstractPacket packet = this.armorstand.createMovePacket( dx, dy, dz );
			players.forEach( player -> packet.sendPacket( player ) );
		}
		
		@Override
		public final void sendVelocityPacket( final Stream<Player> players, final double vx, final double vy,
				final double vz ) {
			final AbstractPacket packet = this.armorstand.createVelocityPacket( vx, vy, vz );
			players.forEach( player -> packet.sendPacket( player ) );
		}
		
		@Override
		public void setName( String name ) {
			this.armorstand.setName( name );
		}
		
	}
	
	private HologramEntity() {
		
	}
	
	@Override
	public void setText( String text ) {
		if ( text != null ) {
			if ( text.length() > MAX_NAME_LENGTH ) {
				throw new IllegalArgumentException( "Line \"" + text + "\" - length exceeds " + MAX_NAME_LENGTH
						+ " characters!" );
			}
		}
		this.setName( text );
		this.text = text;
	}
	
	@Override
	public String getText() {
		return text;
	}
	
	public abstract void setName( String name );
	
}
