package de.mickare.LightHoloDisplayAPI.hologram.wrapper;

import org.bukkit.entity.EntityType;

public class ObjectEntityPacketWrapper extends AbstractObjectEntityPacketWrapper {

	public ObjectEntityPacketWrapper(final int entityId, final EntityType type) {
		super( entityId, type );
		if (type.isAlive() || !type.isSpawnable()) {
			throw new IllegalArgumentException( "Wrong EntityWrapper for EntityType + " + type.name() + "!" );
		}
	}

	public ObjectEntityPacketWrapper(final int entityId, final EntityType type, final int objectData) {
		super( entityId, type, objectData );
		if (type.isAlive() || !type.isSpawnable()) {
			throw new IllegalArgumentException( "Wrong EntityWrapper for EntityType + " + type.name() + "!" );
		}
	}

	public ObjectEntityPacketWrapper(final int entityId, final int typeId) {
		super( entityId, typeId );
	}

	public ObjectEntityPacketWrapper(final int entityId, final int typeId, final int objectData) {
		super( entityId, typeId, objectData );
	}

	// ********************************
	// PACKET

	

}
