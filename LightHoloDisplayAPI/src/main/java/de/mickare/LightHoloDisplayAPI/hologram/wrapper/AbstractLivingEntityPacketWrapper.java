package de.mickare.LightHoloDisplayAPI.hologram.wrapper;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;

import com.comphenix.packetwrapper.WrapperPlayServerEntityMetadata;
import com.comphenix.packetwrapper.WrapperPlayServerSpawnEntityLiving;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.comphenix.protocol.wrappers.WrappedWatchableObject;

public abstract class AbstractLivingEntityPacketWrapper extends AbstractEntityPacketWrapper {
	
	private byte statusByte = 0x0000;
	
	public static enum StatusMask {
		ONFIRE( 0x01 ),
		CROUCHED( 0x02 ),
		UNKNOWN1( 0x04 ),
		SPRINTING( 0x08 ),
		ACTION( 0x10 ),
		INVISIBLE( 0x20 ),
		UNKNOWN2( 0x40 ),
		UNKNOWN3( 0x80 );
		
		public final byte mask;
		
		private StatusMask( int mask ) {
			this.mask = ( byte ) mask;
		}
		
	}
	
	private final WrappedDataWatcher watcher;
	
	@SuppressWarnings( "deprecation" )
	public AbstractLivingEntityPacketWrapper( final int entityId, final EntityType type,
			final WrappedDataWatcher watcher ) {
		super( entityId, type.getTypeId() );
		this.watcher = watcher;
	}
	
	public AbstractLivingEntityPacketWrapper( final int entityId, final int typeId, final WrappedDataWatcher watcher ) {
		super( entityId, typeId );
		this.watcher = watcher;
	}
	
	public final WrappedDataWatcher getDefaultWatcher() {
		return this.watcher;
	}
	
	public final boolean hasDefaultWatcher() {
		return getDefaultWatcher() != null;
	}
	
	public final void setStatusByte( final boolean status, final byte mask ) {
		statusByte = ( byte ) ( status ? ( statusByte ^ mask ) : ( statusByte & ~mask ) );
		if ( this.hasDefaultWatcher() ) {
			this.getDefaultWatcher().setObject( 0, statusByte );
		}
	}
	
	public void setOnFire( final boolean onfire ) {
		setStatusByte( onfire, StatusMask.ONFIRE.mask );
	}
	
	public void setCrouched( final boolean crouched ) {
		setStatusByte( crouched, StatusMask.CROUCHED.mask );
	}
	
	public void setSprinting( final boolean sprinting ) {
		setStatusByte( sprinting, StatusMask.SPRINTING.mask );
	}
	
	public void setDoingAction( final boolean action ) {
		setStatusByte( action, StatusMask.ACTION.mask );
	}
	
	public void setInvisible( final boolean invisible ) {
		setStatusByte( invisible, StatusMask.INVISIBLE.mask );
	}
	
	public void setAir( final short air ) {
		if ( this.hasDefaultWatcher() ) {
			this.getDefaultWatcher().setObject( 1, air );
		}
	}
	
	// ********************************
	// PACKET
	
	public abstract WrapperPlayServerSpawnEntityLiving createSpawnPacket( final Location loc );
	
	public abstract WrapperPlayServerSpawnEntityLiving createSpawnPacket( final Location loc,
			final WrappedDataWatcher watcher );
	
	public abstract WrapperPlayServerSpawnEntityLiving createSpawnPacket( double x, double y, double z, float pitch,
			float yaw );
	
	public abstract WrapperPlayServerSpawnEntityLiving createSpawnPacket( double x, double y, double z, float pitch,
			float yaw, WrappedDataWatcher watcher );
	
	public final WrapperPlayServerEntityMetadata createMetaPacket() {
		if ( this.hasDefaultWatcher() ) {
			return createMetaPacket( this.getDefaultWatcher().getWatchableObjects() );
		} else {
			return createMetaPacket( null );
		}
	}
	
	public final WrapperPlayServerEntityMetadata createMetaPacket( final List<WrappedWatchableObject> value ) {
		final WrapperPlayServerEntityMetadata w = new WrapperPlayServerEntityMetadata();
		w.setEntityId( this.getEntityId() );
		if ( value != null ) {
			w.setMetadata( value );
		}
		return w;
		
	}
	
}
