package de.mickare.LightHoloDisplayAPI.hologram;

import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.springframework.util.ClassUtils;

import de.mickare.LightHoloDisplayAPI.LHDAPI;
import de.mickare.LightHoloDisplayAPI.api.IHologram;
import de.mickare.LightHoloDisplayAPI.reflection.ReflectionUtil;

public class HologramManager {
	
	public static enum VERSION {
		v1_7_R2( "net.minecraft.server.v1_7_R2" ),
		v1_7_R3( "net.minecraft.server.v1_7_R3" ),
		v1_7_R4( "net.minecraft.server.v1_7_R4" ),
		v1_8_R1( "net.minecraft.server.v1_8_R1" ),
		v1_8_R2( "net.minecraft.server.v1_8_R2" ),
		v1_8_R3( "net.minecraft.server.v1_8_R3" ),
		other();
		
		private final String path;
		private final boolean testClass;
		
		private VERSION() {
			this.path = "";
			testClass = false;
		}
		
		private VERSION( String path ) {
			this.path = path;
			testClass = true;
		}
		
		public String getPath() {
			return this.path;
		}
		
		public boolean doTestClass() {
			return testClass;
		}
	}
	
	private static volatile int entityIdCounterBackUp = 9000;
	
	private VERSION version;
	private Class<?> version_entityClass;
	
	public HologramManager() {
		final Entry<VERSION, Class<?>> detect = detectVersion();
		this.version = detect.getKey();
		this.version_entityClass = detect.getValue();
		
	}
	
	public VERSION getVersion() {
		return version;
	}
	
	private Class<?> getVersion_entityClass() {
		return version_entityClass;
	}
	
	private void clearVersion() {
		this.version = VERSION.other;
	}
	
	private static final Entry<VERSION, Class<?>> detectVersion() {
		for ( VERSION v : VERSION.values() ) {
			if ( v.doTestClass() ) {
				if ( ClassUtils.isPresent( v.getPath() + ".Entity", HologramManager.class.getClassLoader() ) ) {
					try {
						return new java.util.AbstractMap.SimpleEntry<VERSION, Class<?>>( v, ClassUtils.forName( v
								.getPath() + ".Entity", HologramManager.class.getClassLoader() ) );
					} catch ( ClassNotFoundException | LinkageError e ) {
						throw new IllegalStateException( e );
					}
				}
			}
		}
		return new java.util.AbstractMap.SimpleEntry<VERSION, Class<?>>( VERSION.other, null );
	}
	
	private final Deque<Integer> freeEntityIds = new LinkedList<Integer>();
	
	private final Set<IHologram> holograms = Collections.newSetFromMap( new WeakHashMap<IHologram, Boolean>() );
	
	public Collection<IHologram> getHolograms() {
		return holograms;
	}
	
	public void updatePlayer( Player player ) {
		synchronized ( holograms ) {
			for ( IHologram h : this.holograms ) {
				h.updatePlayer( player );
			}
		}
	}
	
	public void removeFromPlayer( Player player ) {
		synchronized ( holograms ) {
			for ( IHologram h : this.holograms ) {
				h.removeFromPlayer( player );
			}
		}
	}
	
	protected void registerHologram( IHologram hologram ) {
		synchronized ( holograms ) {
			this.holograms.add( hologram );
		}
	}
	
	protected boolean unregisterHologram( IHologram oldHolo ) {
		synchronized ( holograms ) {
			return this.holograms.remove( oldHolo );
		}
	}
	
	private int generateFreeIds( final Location loc ) {
		for ( int i = 0; i < 20 && this.freeEntityIds.size() < 100; i++ ) {
			this.freeEntityIds.add( this._generateFreeId( loc ) );
		}
		return this._generateFreeId( loc );
	}
	
	private int _generateFreeId( final Location loc ) {
		
		if ( this.getVersion() != VERSION.other && this.getVersion_entityClass() != null ) {
			try {
				return ReflectionUtil.getAndIncrementStaticIntValue( this.version_entityClass, "entityCount", 1 );
			} catch ( Exception e ) {
				LHDAPI.debug( "Warning!  Fallback to classic entityID generation.\nReason: " + e.toString() );
				this.clearVersion();
			}
		}
		try {
			Entity e = loc.getWorld().spawnEntity( new Location( loc.getWorld(), 0, 256, 0 ), EntityType.ARROW );
			int result = e.getEntityId();
			e.remove();
			return result;
		} catch ( Exception e ) {
			LHDAPI.debug( "Warning!  Classic entityID generation failed.\nReason: " + e.toString() );
		}
		return entityIdCounterBackUp++;
	}
	
	public int getNextFreeId( final Location loc ) {
		Integer result = freeEntityIds.poll();
		if ( result != null ) {
			return result.intValue();
		}
		synchronized ( this ) {
			// Try again, it could have changed!
			result = freeEntityIds.poll();
			if ( result != null ) {
				return result.intValue();
			}
			// What if it is the server main thread?
			if ( Bukkit.isPrimaryThread() ) {
				return generateFreeIds( loc );
			}
			// Try a sync call
			try {
				return Bukkit.getScheduler().callSyncMethod( LHDAPI.getPlugin(), ( ) -> generateFreeIds( loc ) )
						.get( 100, TimeUnit.MILLISECONDS );
			} catch ( TimeoutException e ) {
				// After 2 ticks of waiting...
				// Force the generation of ids, even if it's not in server thread!
				return generateFreeIds( loc );
			} catch ( Exception e ) {
				LHDAPI.getPlugin().getLogger()
						.log( Level.WARNING, "Exception while generating a free hologram entity id!", e );
				return entityIdCounterBackUp++;
			}
			
		}
		
	}
	
	public void addFreeId( int id ) {
		freeEntityIds.add( id );
	}
	
}
