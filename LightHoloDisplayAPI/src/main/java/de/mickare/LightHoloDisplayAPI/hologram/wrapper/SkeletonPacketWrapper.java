package de.mickare.LightHoloDisplayAPI.hologram.wrapper;

import org.bukkit.entity.EntityType;

import com.comphenix.protocol.wrappers.WrappedDataWatcher;

public class SkeletonPacketWrapper extends LivingEntityPacketWrapper {

    private static WrappedDataWatcher watcherCache = null;

    public static final WrappedDataWatcher getNewWatcher() {
        if (watcherCache != null) {
            return watcherCache.deepClone();
        }
        // final World w = Bukkit.getWorlds().get( 0 );
        // final Entity e = w.spawnEntity( w.getSpawnLocation().clone().add( 0,
        // 255, 0 ), EntityType.HORSE );
        // watcherCache = WrappedDataWatcher.getEntityWatcher( e ).deepClone();
        // e.remove();

        watcherCache = new WrappedDataWatcher();
        // Entity
        watcherCache.setObject(0, (byte) 0); // fire/crouched/...
        watcherCache.setObject(1, (short) 10); // air
        // Living Entity
        watcherCache.setObject(2, "Zombie"); // custom name
        watcherCache.setObject(3, (byte) 0); // use custom

        watcherCache.setObject(6, (float) 20); // health
        watcherCache.setObject(7, (int) 0); // potion
        watcherCache.setObject(8, (byte) 0); //potion ambient
        watcherCache.setObject(9, (byte) 0); // arrows in entity
        watcherCache.setObject(15, (byte) 0); // no ai
        // Horse
        watcherCache.setObject(13, (byte) 0); // wither

        return watcherCache.deepClone();
    }

    public SkeletonPacketWrapper(int entityId, int typeId, WrappedDataWatcher watcher) {
        super(entityId, typeId, watcher);
        // TODO Auto-generated constructor stub
    }

    private boolean wither = false;

    public SkeletonPacketWrapper(final int entityId) {
        this(entityId, getNewWatcher());
    }

    public SkeletonPacketWrapper(final int entityId, final WrappedDataWatcher watcher) {
        super(entityId, EntityType.SKELETON, watcher);
    }

    public boolean isWither() {
        return wither;
    }

    public void setWither(boolean wither) {
        this.wither = wither;
        this.getDefaultWatcher().setObject(13, (byte)(wither ? 0 : 1));
    }

    

}
