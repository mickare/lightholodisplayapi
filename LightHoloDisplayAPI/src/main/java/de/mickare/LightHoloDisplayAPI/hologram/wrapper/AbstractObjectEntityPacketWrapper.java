package de.mickare.LightHoloDisplayAPI.hologram.wrapper;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;

import com.comphenix.packetwrapper.WrapperPlayServerSpawnEntity;

public abstract class AbstractObjectEntityPacketWrapper extends AbstractEntityPacketWrapper {
	
	@SuppressWarnings( "deprecation" )
	public static final int getObjectID( final EntityType type ) {
		switch ( type ) {
			case BOAT:
				return 1;
			case DROPPED_ITEM:
				return 2;
			case MINECART:
			case MINECART_CHEST:
			case MINECART_COMMAND:
			case MINECART_FURNACE:
			case MINECART_HOPPER:
			case MINECART_MOB_SPAWNER:
			case MINECART_TNT:
				return 10;
			case PRIMED_TNT:
				return 50;
			case ENDER_CRYSTAL:
				return 51;
			case ARROW:
				return 60;
			case SNOWBALL:
				return 61;
			case EGG:
				return 62;
			case FIREBALL:
				return 63;
			case SMALL_FIREBALL:
				return 64;
			case ENDER_PEARL:
				return 65;
			case WITHER_SKULL:
				return 66;
			case FALLING_BLOCK:
				return 70;
			case ITEM_FRAME:
				return 71;
			case ENDER_SIGNAL:
				return 72;
			case SPLASH_POTION:
				return 73;
			case THROWN_EXP_BOTTLE:
				return 75;
			case FISHING_HOOK:
				return 90;
			default:
				return type.getTypeId();
		}
		
	}
	
	private int objectData = 0;
	
	public AbstractObjectEntityPacketWrapper( final int entityId, final EntityType type ) {
		this( entityId, getObjectID( type ) );
	}
	
	public AbstractObjectEntityPacketWrapper( final int entityId, final int typeId ) {
		this( entityId, typeId, 0 );
	}
	
	public AbstractObjectEntityPacketWrapper( final int entityId, final EntityType type, final int objectData ) {
		this( entityId, getObjectID( type ), objectData );
	}
	
	public AbstractObjectEntityPacketWrapper( final int entityId, final int typeId, final int objectData ) {
		super( entityId, typeId );
		this.objectData = objectData;
	}
	
	// ********************************
	// PACKET
	
	@Override
	public final WrapperPlayServerSpawnEntity createSpawnPacket( final Location loc ) {
		if ( this.hasDefaultObjectData() ) {
			return createSpawnPacket( loc, 0 );
		} else {
			return createSpawnPacket( loc, this.getObjectData() );
		}
	}
	
	public final WrapperPlayServerSpawnEntity createSpawnPacket( final Location loc, final int objectData ) {
		final WrapperPlayServerSpawnEntity w = new WrapperPlayServerSpawnEntity();
		w.setEntityID( this.getEntityId() );
		w.setType( this.getTypeId() );
		w.setX( loc.getX() );
		w.setY( loc.getY() );
		w.setZ( loc.getZ() );
		w.setPitch( loc.getPitch() );
		w.setYaw( loc.getYaw() );
		w.setObjectData( objectData );
		return w;
	}
	
	public final boolean hasDefaultObjectData() {
		return objectData != 0;
	}
	
	public final int getObjectData() {
		return objectData;
	}
	
	public final void setObjectData( final int objectData ) {
		this.objectData = objectData;
	}
	
}
