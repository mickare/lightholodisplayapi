package de.mickare.LightHoloDisplayAPI.hologram.wrapper;

import java.util.Collection;

import org.bukkit.Location;

import com.comphenix.packetwrapper.AbstractPacket;
import com.comphenix.packetwrapper.WrapperPlayServerAttachEntity;
import com.comphenix.packetwrapper.WrapperPlayServerEntityDestroy;
import com.comphenix.packetwrapper.WrapperPlayServerEntityLook;
import com.comphenix.packetwrapper.WrapperPlayServerEntityMoveLook;
import com.comphenix.packetwrapper.WrapperPlayServerEntityTeleport;
import com.comphenix.packetwrapper.WrapperPlayServerEntityVelocity;
import com.comphenix.packetwrapper.WrapperPlayServerRelEntityMove;
import com.google.common.primitives.Ints;

public abstract class AbstractEntityPacketWrapper {
	
	private final int entityId;
	private final int typeId;
	
	public AbstractEntityPacketWrapper( final int entityId, final int typeId ) {
		// Validate.notNull( watcher );
		this.entityId = entityId;
		this.typeId = typeId;
	}
	
	// ********************************
	// GETTER
	
	public final int getEntityId() {
		return entityId;
	}
	
	public final int getTypeId() {
		return typeId;
	}
	
	// ********************************
	// PACKET
	public abstract AbstractPacket createSpawnPacket( final Location loc );
	
	public final WrapperPlayServerEntityDestroy createDestroyPacket() {
		return AbstractEntityPacketWrapper.createDestroyPacket( this.getEntityId() );
	}
	
	public static final WrapperPlayServerEntityDestroy createDestroyPacket( final int... entityIds ) {
		WrapperPlayServerEntityDestroy w = new WrapperPlayServerEntityDestroy();
		w.setEntityIds( entityIds );
		return w;
	}
	
	public static final WrapperPlayServerEntityDestroy createDestroyPacket( final Collection<Integer> entityIds ) {
		return createDestroyPacket( Ints.toArray( entityIds ) );
	}
	
	public final WrapperPlayServerEntityTeleport createTeleportPacket( final Location loc ) {
		return this.createTeleportPacket( loc.getX(), loc.getY(), loc.getZ(), loc.getPitch(), loc.getYaw() );
	}
	
	public final WrapperPlayServerEntityTeleport createTeleportPacket( final double x, final double y, final double z,
			final float pitch, final float yaw ) {
		final WrapperPlayServerEntityTeleport w = new WrapperPlayServerEntityTeleport();
		w.setEntityID( this.getEntityId() );
		w.setX( x );
		w.setY( y );
		w.setZ( z );
		w.setPitch( pitch );
		w.setYaw( yaw );
		return w;
	}
	
	public final WrapperPlayServerRelEntityMove createMovePacket( final double dx, final double dy, final double dz ) {
		final WrapperPlayServerRelEntityMove w = new WrapperPlayServerRelEntityMove();
		w.setEntityId( this.getEntityId() );
		w.setDx( dx );
		w.setDy( dy );
		w.setDz( dz );
		return w;
	}
	
	public final WrapperPlayServerEntityLook createLookPacket( final float yaw, final float pitch ) {
		final WrapperPlayServerEntityLook w = new WrapperPlayServerEntityLook();
		w.setEntityId( this.getEntityId() );
		w.setYaw( yaw );
		w.setPitch( pitch );
		return w;
	}
	
	public final WrapperPlayServerEntityMoveLook createMoveAndLookPacket( final double dx, final double dy,
			final double dz, final float yaw, final float pitch ) {
		final WrapperPlayServerEntityMoveLook w = new WrapperPlayServerEntityMoveLook();
		w.setEntityId( this.getEntityId() );
		w.setDx( dx );
		w.setDy( dy );
		w.setDz( dz );
		w.setYaw( yaw );
		w.setPitch( pitch );
		return w;
	}
	
	public final WrapperPlayServerEntityVelocity createVelocityPacket( final double vx, final double vy, final double vz ) {
		final WrapperPlayServerEntityVelocity w = new WrapperPlayServerEntityVelocity();
		w.setEntityId( this.getEntityId() );
		w.setVelocityX( vx );
		w.setVelocityY( vy );
		w.setVelocityZ( vz );
		return w;
	}
	
	public final WrapperPlayServerAttachEntity createAttachPacket( final int vehicleEntityID ) {
		return createAttachPacket( vehicleEntityID, false );
	}
	
	public final WrapperPlayServerAttachEntity createAttachPacket( final int vehicleEntityID, final boolean leached ) {
		final WrapperPlayServerAttachEntity w = new WrapperPlayServerAttachEntity();
		w.setEntityId( this.getEntityId() );
		w.setVehicleId( vehicleEntityID );
		w.setLeash( leached );
		return w;
	}
	
}
