package de.mickare.LightHoloDisplayAPI.hologram.wrapper;

import com.comphenix.protocol.wrappers.WrappedDataWatcher;

public class ArmorStandPacketWrapper extends LivingEntityPacketWrapper {
	
	public static final int ARMORSTAND_NETWORK_ID = 30;
	
	private static WrappedDataWatcher watcherCache = null;
	
	public static final WrappedDataWatcher getNewWatcher() {
		if ( watcherCache != null ) {
			return watcherCache.deepClone();
		}
		
		watcherCache = new WrappedDataWatcher();
		// Entity
		watcherCache.setObject( 0, ( byte ) 0 );
		watcherCache.setObject( 1, ( short ) 10 );
		// Living Entity
		watcherCache.setObject( 2, "ArmorStand" );
		watcherCache.setObject( 3, ( byte ) 0 );
		watcherCache.setObject( 6, ( float ) 20 );
		watcherCache.setObject( 7, ( int ) 0 );
		watcherCache.setObject( 8, ( byte ) 0 );
		watcherCache.setObject( 9, ( byte ) 0 );
		// watcherCache.setObject( 15, (byte) 0 ); // Disabled because 15 in 1.8.0 = left leg position
		
		// ArmorStand
		watcherCache.setObject( 10, ( byte ) 0 );
		
		return watcherCache.deepClone();
	}
	
	public static enum ArmorStandStatusMask {
		SMALL_ARMORSTAND( 0x01 ),
		HAS_GRAVITY( 0x02 ),
		HAS_ARMS( 0x04 ),
		HAS_BASEPLATE( 0x08 );
		
		public final byte mask;
		
		private ArmorStandStatusMask( int mask ) {
			this.mask = ( byte ) mask;
		}
		
	}
	
	private byte armorStandStatus = 0x00;
	
	public ArmorStandPacketWrapper( int entityId ) {
		super( entityId, ARMORSTAND_NETWORK_ID, getNewWatcher() );
	}
	
	public final void setArmorStandStatus( final boolean status, final byte mask ) {
		armorStandStatus = ( byte ) ( status ? ( armorStandStatus ) : ( armorStandStatus & ~mask ) );
		if ( this.hasDefaultWatcher() ) {
			this.getDefaultWatcher().setObject( 10, armorStandStatus );
		}
	}
	
	public final void setSmallArmorStand( boolean b ) {
		this.setArmorStandStatus( b, ArmorStandStatusMask.SMALL_ARMORSTAND.mask );
	}
	
	public final void setHasGravity( boolean b ) {
		this.setArmorStandStatus( b, ArmorStandStatusMask.HAS_GRAVITY.mask );
	}
	
	public final void setHasArms( boolean b ) {
		this.setArmorStandStatus( b, ArmorStandStatusMask.HAS_ARMS.mask );
	}
	
	public final void setHasBaseplate( boolean b ) {
		this.setArmorStandStatus( b, ArmorStandStatusMask.HAS_BASEPLATE.mask );
	}
}
