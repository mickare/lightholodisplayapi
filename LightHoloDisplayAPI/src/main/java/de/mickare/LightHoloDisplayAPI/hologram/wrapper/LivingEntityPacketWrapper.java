package de.mickare.LightHoloDisplayAPI.hologram.wrapper;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.util.Vector;

import com.comphenix.packetwrapper.WrapperPlayServerSpawnEntityLiving;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;

public class LivingEntityPacketWrapper extends AbstractLivingEntityPacketWrapper {

	private Vector defaultVelocity = null;

	public LivingEntityPacketWrapper(final int entityId, final EntityType type, final WrappedDataWatcher watcher) {
		super( entityId, type, watcher );
		if (!type.isAlive() || !type.isSpawnable()) {
			throw new IllegalArgumentException( "Wrong EntityWrapper for EntityType + " + type.name() + "!" );
		}
	}

	public LivingEntityPacketWrapper(final int entityId, final int typeId, final WrappedDataWatcher watcher) {
		super( entityId, typeId, watcher );
	}

	public final void setHealth( final float health ) {
		if (this.hasDefaultWatcher()) {
			this.getDefaultWatcher().setObject( 6, health );
		}
	}

	public final void setPotionEffectColor( final int color ) {
		if (this.hasDefaultWatcher()) {
			this.getDefaultWatcher().setObject( 7, color );
		}
	}

	public final void setPotionEffectAmbient( final byte ambient ) {
		if (this.hasDefaultWatcher()) {
			this.getDefaultWatcher().setObject( 8, ambient );
		}
	}

	public final void setNumbersOfArrowsInEntity( final byte number ) {
		if (this.hasDefaultWatcher()) {
			this.getDefaultWatcher().setObject( 9, number );
		}
	}

	public final void setNoAi( final boolean noai ) {
		if (this.hasDefaultWatcher()) {
			this.getDefaultWatcher().setObject( 15, noai ? 1 : 0 );
		}
	}

	public void setName( final String name ) {
		if (this.hasDefaultWatcher()) {
			final WrappedDataWatcher watcher = this.getDefaultWatcher();
			if (name != null) {
				watcher.setObject( 2, (String) name );
				watcher.setObject( 3, (byte) 1 );
			} else {
				watcher.setObject( 2, (String) "" );
				watcher.setObject( 3, (byte) 0 );
			}
		}
	}

	// ********************************
	// PACKET

	@Override
	public final WrapperPlayServerSpawnEntityLiving createSpawnPacket( final Location loc ) {
		return createSpawnPacket( loc, this.getDefaultWatcher() );
	}

	@Override
	public final WrapperPlayServerSpawnEntityLiving createSpawnPacket( final Location loc, final WrappedDataWatcher watcher ) {
		return createSpawnPacket( loc, watcher, 0 );
	}

	@Override
	public final WrapperPlayServerSpawnEntityLiving createSpawnPacket( final double x, final double y, final double z, final float pitch,
			final float yaw ) {
		return createSpawnPacket( x, y, z, pitch, yaw, this.getDefaultWatcher() );
	}

	@Override
	public final WrapperPlayServerSpawnEntityLiving createSpawnPacket( final double x, final double y, final double z, final float pitch,
			final float yaw, final WrappedDataWatcher watcher ) {
		return createSpawnPacket( x, y, z, pitch, yaw, watcher, 0, getDefaultVelocity() );
	}

	public final WrapperPlayServerSpawnEntityLiving createSpawnPacket( final Location loc, final WrappedDataWatcher watcher,
			final float headPitch ) {
		return createSpawnPacket( loc, watcher, loc.getYaw(), getDefaultVelocity() );
	}

	public final WrapperPlayServerSpawnEntityLiving createSpawnPacket( final Location loc, final WrappedDataWatcher watcher,
			final float bodyyaw, final Vector v ) {
		return this.createSpawnPacket( loc.getX(), loc.getY(), loc.getZ(), loc.getPitch(), loc.getYaw(), watcher, bodyyaw, v );
	}

	public final WrapperPlayServerSpawnEntityLiving createSpawnPacket( final double x, final double y, final double z, final float pitch,
			final float yaw, final WrappedDataWatcher watcher, final float bodyyaw, final Vector v ) {
		final WrapperPlayServerSpawnEntityLiving w = new WrapperPlayServerSpawnEntityLiving();
		w.setEntityID( this.getEntityId() );
		w.getHandle().getIntegers().write( 1, this.getTypeId() );
		w.setX( x );
		w.setY( y );
		w.setZ( z );
		w.setHeadPitch( pitch );
		w.setYaw( bodyyaw );
		w.setHeadYaw( yaw );

		if (v != null) {
			w.setVelocityX( v.getX() );
			w.setVelocityY( v.getY() );
			w.setVelocityZ( v.getZ() );
		}
		if (watcher != null) {
			w.setMetadata( watcher );
		}

		return w;
	}

	public final Vector getDefaultVelocity() {
		return defaultVelocity;
	}

	public final void setDefaultVelocity( final Vector defaultVelocity ) {
		this.defaultVelocity = defaultVelocity;
	}

}
