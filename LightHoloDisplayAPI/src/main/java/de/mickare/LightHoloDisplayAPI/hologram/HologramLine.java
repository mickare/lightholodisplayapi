package de.mickare.LightHoloDisplayAPI.hologram;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.stream.Stream;

import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import de.mickare.LightHoloDisplayAPI.api.IHologramLine;

public final class HologramLine implements IHologramLine {
	
	public final static boolean canBeMoved( Vector delta ) {
		return canBeMoved( delta.getX(), delta.getY(), delta.getZ() );
	}
	
	public final static boolean canBeMoved( final double dx, final double dy, final double dz ) {
		return canBeMoved( dx ) && canBeMoved( dy ) && canBeMoved( dz );
	}
	
	private final static boolean canBeMoved( final double delta ) {
		return -4 <= delta && delta < 4;
	}
	
	public final static double translateMovePrecision( double delta ) {
		return ( ( byte ) ( delta * 32 ) ) / 32D;
	}
	
	public final static double velocityToBlock( double velocity ) {
		return ( ( double ) ( ( short ) ( ( int ) ( velocity * 8000 ) ) ) ) / 8000;
	}
	
	public static final int MAX_LINE_LENGTH = 300;
	
	private final HologramEntity entity;
	
	private double locX, locY, locZ;
	private double offsetY = 0;
	
	private boolean state_destroyed = false;
	private boolean state_changedText = false;
	
	private final Set<Player> players = Collections.newSetFromMap( new WeakHashMap<Player, Boolean>() );
	
	public HologramLine( final int entityId, double x, double y, double z ) {
		this.entity = new HologramEntity.ArmorStandEntity( entityId );
		this.locX = x;
		this.locY = y;
		this.locZ = z;
	}
	
	private void checkState() {
		if ( state_destroyed ) {
			throw new IllegalStateException( "HologramLine is destroyed!" );
		}
	}
	
	@Override
	public void moveOrTeleportTo( final double locX, final double locY, final double locZ ) {
		double dx = locX - this.locX, dy = locY - this.locY, dz = locZ - this.locZ;
		if ( canBeMoved( dx, dy, dz ) ) {
			this.move( dx, dy, dz );
		} else {
			this.teleport( locX, locY, locZ );
		}
	}
	
	@Override
	public void moveOrTeleportDelta( final double dx, final double dy, final double dz ) {
		if ( canBeMoved( dx, dy, dz ) ) {
			this.move( dx, dy, dz );
		} else {
			this.teleport( locX + dx, locY + dy, locZ + dz );
		}
	}
	
	@Override
	public synchronized void teleport( final double locX, final double locY, final double locZ ) {
		checkState();
		this.locX = locX;
		this.locY = locY;
		this.locZ = locZ;
		if ( this.players.size() > 0 )
			this.entity.sendTeleportPacket( this.players.stream(), locX, locY + this.offsetY, locZ );
	}
	
	@Override
	public synchronized void move( final double dx, final double dy, final double dz ) {
		checkState();
		if ( !canBeMoved( dx, dy, dz ) ) {
			throw new IllegalArgumentException( "Displacement cannot exceed 4 meters." );
		}
		final double tdx = translateMovePrecision( dx );
		final double tdy = translateMovePrecision( dy );
		final double tdz = translateMovePrecision( dz );
		if ( tdx == 0 && tdy == 0 && tdz == 0 ) {
			return;
		}
		this.locX += tdx;
		this.locY += tdy;
		this.locZ += tdz;
		
		if ( this.players.size() > 0 )
			this.entity.sendRelMovePacket( this.players.stream(), tdx, tdy, tdz );
	}
	
	@Override
	public synchronized void velocity( double vx, double vy, double vz ) {
		checkState();
		this.locX += velocityToBlock( vx );
		this.locY += velocityToBlock( vy );
		this.locZ += velocityToBlock( vz );
		
		if ( this.players.size() > 0 )
			this.entity.sendVelocityPacket( this.players.stream(), vx, vy, vz );
	}
	
	@Override
	public final String getText() {
		return this.entity.getText();
	}
	
	private boolean stringEquals( String a, String b ) {
		return ( a != null ) ? a.equals( b ) : b == null;
	}
	
	@Override
	public synchronized final void setText( final String text ) {
		checkState();
		this.state_changedText = this.state_changedText || !stringEquals( this.entity.getText(), text );
		this.entity.setText( text );
	}
	
	@Override
	public synchronized void spawnToAll( Collection<Player> players ) {
		checkState();
		if ( players.isEmpty() ) {
			return;
		}
		this.entity.sendSpawnPacket( players.stream().filter( p -> !this.players.contains( p ) ), this.locX, this.locY
				+ this.offsetY, this.locZ );
		this.players.addAll( players );
	}
	
	@Override
	public synchronized void spawnTo( Player player ) {
		checkState();
		if ( this.players.add( player ) ) {
			this.entity.sendSpawnPacket( Stream.of( player ), this.locX, this.locY + this.offsetY, this.locZ );
		}
	}
	
	@Override
	public synchronized void despawnToAll() {
		checkState();
		if ( !this.players.isEmpty() ) {
			this.entity.sendDestroyPacket( this.players.stream() );
			this.players.clear();
		}
	}
	
	@Override
	public synchronized void despawnTo( Player player ) {
		checkState();
		if ( this.players.remove( player ) ) {
			this.entity.sendDestroyPacket( Stream.of( player ) );
		}
	}
	
	@Override
	public synchronized void updateText() {
		checkState();
		if ( this.state_changedText ) {
			this.state_changedText = false;
			if ( this.players.size() > 0 )
				this.entity.sendMetadataPacket( this.players.stream() );
		}
	}
	
	@Override
	public synchronized void destroy( HologramManager manager ) {
		checkState();
		this.state_destroyed = true;
		if ( this.players.size() > 0 )
			this.entity.sendDestroyPacket( this.players.stream() );
		this.players.clear();
		for ( Integer i : this.entity.getEntityIds() ) {
			manager.addFreeId( i );
		}
	}
	
	public double getOffsetY() {
		return offsetY;
	}
	
	public void setOffsetY( double offsetY ) {
		this.offsetY = offsetY;
	}
	
}
