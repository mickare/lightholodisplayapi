package de.mickare.LightHoloDisplayTEST;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.google.common.collect.Range;

import de.mickare.LightHoloDisplayAPI.LHDAPI;
import de.mickare.LightHoloDisplayAPI.animation.EASING;
import de.mickare.LightHoloDisplayAPI.animation.position.AbstractOffsetElement;
import de.mickare.LightHoloDisplayAPI.animation.position.AnimationPosition;
import de.mickare.LightHoloDisplayAPI.animation.position.PositionHologramAnimation;
import de.mickare.LightHoloDisplayAPI.animation.position.SimpleOffsetChainRun;
import de.mickare.LightHoloDisplayAPI.animation.position.presets.LinearOffset;
import de.mickare.LightHoloDisplayAPI.animation.text.TextChainAnimation;
import de.mickare.LightHoloDisplayAPI.api.IHologram;
import de.mickare.LightHoloDisplayAPI.hologram.Hologram;
import de.mickare.LightHoloDisplayAPI.util.SimpleScheduledService;
import de.mickare.org.apache.commons.math.geometry.Vector3D;

public class TestListener implements Listener {
	private final TestPlugin plugin;

	private final TextChainAnimation textChain;

	public TestListener(TestPlugin plugin) {
		this.plugin = plugin;

		textChain = TextChainAnimation.builder( "Hologram", null, ChatColor.RED + "" + ChatColor.BOLD + "Test" );
		textChain.text( 20, "Hologram", null, "WTF" );
		textChain.text( 20, "KLAPPT" );
		textChain.text( 20, "Oder doch", "nicht?" );
		for (int i = 30; i > 0; i--) {
			textChain.text( 5, String.valueOf( i ) );
		}
		textChain.loop();

	}

	@EventHandler
	public void onJoin( PlayerJoinEvent event ) {

		final Player player = event.getPlayer();

		final IHologram hologram = Hologram.newBuilder( player.getLocation() ).setBounding( Hologram.BOUND.MIDDLE ).build();
		hologram.setText( "Hologram", null, ChatColor.RED + "" + ChatColor.BOLD + "Test" );

		final PositionHologramAnimation animation = new PositionHologramAnimation( hologram,
				SimpleScheduledService.Scheduler.timerAsynchronously( plugin, 0, 1 ) );

		// Set Position to player position
		animation.setPosition( new AnimationPosition() {
			@Override
			public Vector3D apply( Long time, Long delta ) {
				final Location l = player.getLocation();
				return new Vector3D( l.getX(), l.getY(), l.getZ() );
			}
		}, Range.atLeast( (long) 250 ) );

		// Keep the hologram in front of player
		final TestLoopOffsetChainRun frontFacing = new TestLoopOffsetChainRun( 3000 );
		frontFacing.getRoot().addChild(
				new AbstractOffsetElement() {
					@Override
					public Vector3D getOffset( double time ) {
						if (time <= 0) {
							return this.getChildrenOffset( time );
						}
						float ryaw = -player.getLocation().getYaw() / 180f * (float) Math.PI;
						double z = Math.cos( ryaw ) * 2;
						double x = Math.sin( ryaw ) * 2;
						return new Vector3D( x, 0, z ).add( this.getChildrenOffset( time ) );
					}
				}.addChild( new LinearOffset( new Vector3D( 0, 1, 0 ), EASING.BOUNCE_INOUT ) ).addChild(
						new LinearOffset( new Vector3D( 0, -1, 0 ), EASING.SINE_INOUT ) ) );
		animation.addOffsetChain( frontFacing, Range.atLeast( (long) 250 ) );

		// Circling arround Player
		SimpleOffsetChainRun circling = new SimpleOffsetChainRun( 1000 );
		circling.getRoot().addChild( new LinearOffset( new Vector3D( 0, 2, 0 ), EASING.CIRC_IN ) );
		animation.addOffsetChain( circling, 0 );

		plugin.holos.put( player, hologram );

		// Start animation
		Bukkit.getScheduler().runTaskLaterAsynchronously( plugin, new Runnable() {
			@Override
			public void run() {
				animation.start();
				hologram.addToPlayer( player );
			}
		}, 15 );

		textChain.start( plugin, hologram );

		/*
		 * new BukkitRunnable() { String[][] messages = { { "Hologram", null, ChatColor.RED + "" + ChatColor.BOLD +
		 * "Test" }, { "Hologram", null, "WTF" }, { "KLAPPT!" }, { "Oder doch", "nicht?" } }; int msg = 0;
		 * 
		 * @Override public void run() { if (!player.isOnline()) { this.cancel(); return; } h.setText( messages[msg] );
		 * msg = (msg + 1) % messages.length; } }.runTaskTimerAsynchronously( plugin, 30, 20 );
		 */

	}

	public void onQuit( PlayerQuitEvent event ) {
		LHDAPI.getManager().removeFromPlayer( event.getPlayer() );
		plugin.holos.invalidate( event.getPlayer() );
	}

}
