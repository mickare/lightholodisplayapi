package de.mickare.LightHoloDisplayTEST.commands;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.mickare.LightHoloDisplayAPI.api.IHologram;
import de.mickare.LightHoloDisplayTEST.TestPlugin;

public class HoloSetLine extends AbstractCommand {

	public HoloSetLine(TestPlugin plugin) {
		super( plugin );
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean onCommand( CommandSender sender, Command cmd, String label, String[] args ) {
		if (!(sender instanceof Player)) {
			sender.sendMessage( "You are no player!" );
			return false;
		}
		IHologram holo = getPlugin().holos.getIfPresent( sender );

		if(holo == null) {
			sender.sendMessage( "No hologram!" );
			return false;
		}
		
		List<String> htext = holo.getText();
		
		int line = 0;
		if (args.length > 0) {
			try {
				line = Integer.parseInt( args[0] );
			} catch (NumberFormatException nfe2) {
				sender.sendMessage( "No line number!" );
				return false;
			}
		}
		String newtextline = null;
		if(args.length > 1) {
			StringBuilder sb = new StringBuilder();
			for(int i = 1; i < args.length; i++) {
				if(i != 1) {
					sb.append( " " );
				}
				sb.append( args[i] );
			}
			newtextline = ChatColor.translateAlternateColorCodes( '&', sb.toString() );
		}
		
		while(line >= htext.size()) {
			htext.add( null );
		}
		
		htext.set( line, newtextline );
		while(htext.size() > 0) {
			if(htext.get( htext.size() - 1 ) != null) {
				break;
			} else {
				htext.remove( htext.size() - 1 );
			}
		}
		holo.setText( htext );

		return true;
	}

}
