package de.mickare.LightHoloDisplayTEST.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.mickare.LightHoloDisplayTEST.TestPlugin;

public abstract class AbstractCommand implements CommandExecutor {

	private final TestPlugin plugin;
	
	public AbstractCommand(TestPlugin plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public abstract boolean onCommand( CommandSender sender, Command cmd, String label, String[] args );

	public TestPlugin getPlugin() {
		return plugin;
	}

}
