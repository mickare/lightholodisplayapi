package de.mickare.LightHoloDisplayTEST.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import de.mickare.LightHoloDisplayAPI.api.IHologram;
import de.mickare.LightHoloDisplayTEST.TestPlugin;

public class HoloMove extends AbstractCommand {

	public HoloMove(TestPlugin plugin) {
		super( plugin );
	}

	@Override
	public boolean onCommand( CommandSender sender, Command cmd, String label, String[] args ) {
		if (!(sender instanceof Player)) {
			sender.sendMessage( "You are no player!" );
			return false;
		}
		Player player = (Player) sender;
		IHologram holo = getPlugin().holos.getIfPresent( sender );

		if (holo == null) {
			sender.sendMessage( "No hologram!" );
			return false;
		}

		double multi = 1;
		if (args.length > 0) {
			try {
				multi = Double.parseDouble( args[0] );
			} catch (NumberFormatException nfe1) {
				try {
					multi = Integer.parseInt( args[0] );
				} catch (NumberFormatException nfe2) {
					sender.sendMessage( "No number!" );
					return false;
				}
			}
		}

		Vector d = player.getLocation().getDirection();

		holo.moveOrTeleportDelta( d.getX() * multi, d.getY() * multi, d.getZ() * multi );

		return true;
	}

}
