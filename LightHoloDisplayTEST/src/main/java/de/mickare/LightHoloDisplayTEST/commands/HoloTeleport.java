package de.mickare.LightHoloDisplayTEST.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.mickare.LightHoloDisplayAPI.api.IHologram;
import de.mickare.LightHoloDisplayTEST.TestPlugin;

public class HoloTeleport extends AbstractCommand {

	public HoloTeleport(TestPlugin plugin) {
		super( plugin );
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean onCommand( CommandSender sender, Command cmd, String label, String[] args ) {
		if(!(sender instanceof Player)) {
			sender.sendMessage( "You are no player!" );
			return false;
		}
		Player player = (Player)sender;
		IHologram holo = getPlugin().holos.getIfPresent( sender );
		
		if(holo == null) {
			sender.sendMessage( "No hologram!" );
			return false;
		}

		holo.teleport( player.getLocation() );
		
		return true;
	}

}
