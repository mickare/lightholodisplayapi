package de.mickare.LightHoloDisplayTEST;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;

import de.mickare.LightHoloDisplayAPI.api.IHologram;
import de.mickare.LightHoloDisplayTEST.commands.*;

public class TestPlugin extends JavaPlugin {

	public final Cache<Player, IHologram> holos = CacheBuilder.newBuilder().removalListener( new RemovalListener<Player, IHologram>() {
		@Override
		public void onRemoval( RemovalNotification<Player, IHologram> n ) {
			if (!n.getValue().isDestroyed()) {
				n.getValue().destroy();
			}
		}
	} ).build();

	@Override
	public void onEnable() {

		Bukkit.getPluginManager().registerEvents( new WtfListener( this ), this );

		this.getCommand( "hmove" ).setExecutor( new HoloMove( this ) );
		this.getCommand( "htele" ).setExecutor( new HoloTeleport( this ) );
		this.getCommand( "hpush" ).setExecutor( new HoloPush( this ) );
		this.getCommand( "hsetline" ).setExecutor( new HoloSetLine( this ) );
	}

	@Override
	public void onDisable() {

	}

}
