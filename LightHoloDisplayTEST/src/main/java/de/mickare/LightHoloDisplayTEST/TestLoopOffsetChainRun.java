package de.mickare.LightHoloDisplayTEST;

import de.mickare.LightHoloDisplayAPI.animation.position.OffsetChainRun;
import de.mickare.LightHoloDisplayAPI.animation.position.OffsetElement;
import de.mickare.LightHoloDisplayAPI.animation.position.presets.DummyOffset;
import de.mickare.org.apache.commons.math.geometry.Vector3D;

public class TestLoopOffsetChainRun implements OffsetChainRun {

	private final OffsetElement root = new DummyOffset();

	private final long moduloLoop;

	public TestLoopOffsetChainRun(long moduloLoop) {
		this.moduloLoop = moduloLoop;
	}

	@Override
	public long getDuration() {
		return Long.MAX_VALUE;
	}

	@Override
	public Vector3D getOffset( long start, long time ) {
		if (time < start) {
			return Vector3D.ZERO;
		}
		return root.getOffset( ((double) time - (double) start) % (double) moduloLoop );
	}

	@Override
	public OffsetElement getRoot() {
		return root;
	}

}
