package de.mickare.LightHoloDisplayTEST.mover;

/*
@Deprecated
public final class HoloGlue implements Runnable {

	private static final int COUNTER_MAX = 1000;
	
	private final TestPlugin plugin;
	private final Player player;
	private final Hologram holo;
	private final Vector offset;

	private final int circle = 6;
	private final int circle_squarred = circle * circle;
	private final double squaredTolerance = 0.005;

	private boolean didLastTimeMove = false;

	private int counter = 0;

	public HoloGlue(final TestPlugin plugin, final Player player, final Hologram holo, final Vector offset) {
		this.plugin = plugin;
		this.player = player;
		this.holo = holo;
		this.offset = offset;
	}

	private final boolean isInTolerance( final Vector a, final Vector b, final double squaredTolerance ) {
		return a.distanceSquared( b ) <= squaredTolerance;
	}

	public final void run() {
		if (!holo.getWorld().equals( player.getWorld() )) {
			return;
		}
		final Vector lookingAt = player.getLocation().getDirection().multiply( circle );
		final Vector center = player.getLocation().toVector().add( offset );
		final Vector target = center.add( lookingAt );

		final Vector current = holo.getLocation().toVector();

		if (!isInTolerance( current, target, squaredTolerance )) {
			if (counter % COUNTER_MAX == 0) {
				counter = 1;
				holo.teleport( player.getWorld(), target.getX(), target.getY(), target.getZ() );
				didLastTimeMove = false;
			} else {
				Vector move = target.subtract( current );
				if (move.lengthSquared() > 16) {
					move = move.normalize().multiply( 4 );
				}
					

				holo.move( move.getX(), move.getY(), move.getZ() );
				didLastTimeMove = true;
				counter++;
			}
		} else if (didLastTimeMove) {
			didLastTimeMove = false;
			counter = 1;
			holo.teleport( player.getWorld(), target.getX(), target.getY(), target.getZ() );
		}

	}

	public final Player getPlayer() {
		return player;
	}

	public final IHologram getHologram() {
		return holo;
	}

	public final void stop() {
		plugin.stopHoloGlueTask( player );
	}

}*/

