package de.mickare.LightHoloDisplayTEST;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerOptions;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.ListeningWhitelist;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.events.PacketListener;
import com.comphenix.protocol.injector.GamePhase;

public class OutPacketListener implements PacketListener {

	private final TestPlugin plugin;

	public OutPacketListener(TestPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public void onPacketSending( PacketEvent event ) {

	}

	@Override
	public void onPacketReceiving( PacketEvent event ) {

	}

	@Override
	public ListeningWhitelist getSendingWhitelist() {
		return ListeningWhitelist.newBuilder().priority( ListenerPriority.NORMAL )
				.types( PacketType.Play.Server.SPAWN_ENTITY_LIVING, PacketType.Play.Server.SPAWN_ENTITY ).gamePhase( GamePhase.PLAYING )
				.options( new ListenerOptions[0] ).build();
	}

	@Override
	public ListeningWhitelist getReceivingWhitelist() {
		return ListeningWhitelist.EMPTY_WHITELIST;
	}

	@Override
	public TestPlugin getPlugin() {
		return plugin;
	}

}