package de.mickare.LightHoloDisplayTEST;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.google.common.collect.Range;

import de.mickare.LightHoloDisplayAPI.LHDAPI;
import de.mickare.LightHoloDisplayAPI.animation.position.AnimationPosition;
import de.mickare.LightHoloDisplayAPI.animation.position.PositionHologramAnimation;
import de.mickare.LightHoloDisplayAPI.api.IHologram;
import de.mickare.LightHoloDisplayAPI.hologram.Hologram;
import de.mickare.LightHoloDisplayAPI.util.SimpleScheduledService;
import de.mickare.org.apache.commons.math.geometry.Vector3D;

public class WtfListener implements Listener {

	private final TestPlugin plugin;

	public WtfListener(TestPlugin plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onJoin( PlayerJoinEvent event ) {
		final Player player = event.getPlayer();

		final IHologram hologram = Hologram.newBuilder( player.getLocation() ).setBounding( Hologram.BOUND.BOTTOM ).build();
		hologram.setText( ChatColor.WHITE + "[" + ChatColor.AQUA + "Developer" + ChatColor.WHITE + "] " + player.getName() );

		final PositionHologramAnimation animation = new PositionHologramAnimation( hologram,
				SimpleScheduledService.Scheduler.timerAsynchronously( plugin, 0, 1 ) );

		// Set Position to player position
		animation.setPosition( new AnimationPosition() {
			@Override
			public Vector3D apply( Long time, Long delta ) {
				final Location l = player.getLocation();
				return new Vector3D( l.getX(), l.getY() + 2.02, l.getZ() );
			}
		}, Range.all() );

		plugin.holos.put( player, hologram );

		// Start animation
		Bukkit.getScheduler().runTaskLaterAsynchronously( plugin, new Runnable() {
			@Override
			public void run() {
				animation.start();
				hologram.addToPlayer( player );
			}
		}, 15 );

	}

	@EventHandler
	public void onQuit( PlayerQuitEvent event ) {
		LHDAPI.getManager().removeFromPlayer( event.getPlayer() );
		plugin.holos.invalidate( event.getPlayer() );
	}

}
